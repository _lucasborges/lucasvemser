
public abstract class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida, qtdDano;
    protected int experiencia, qtdExperienciaPorAtaque;    

    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario(0);
        experiencia =0;
        qtdExperienciaPorAtaque = 1;
        qtdDano = 0.0;
    }    

    public Personagem( String nome ) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome() {
        this.nome = nome;
    }

    public Status getStatus() {
        return this.status;
    }

    public int getExperiencia() {
        return this.experiencia;
    }

    public double getVida() {
        return this.vida;
    } 

    public Inventario getInventario() {
        return this.inventario;
    }

    public void ganharItem( Item item ) {
        this.inventario.adicionar(item);
    }

    public void perderItem( Item item ) {
        this.inventario.remover(item);
    }

    public boolean podeSofrerDano() {
        return this.vida > 0;
    }

    public void aumentarXp(){
        experiencia = experiencia + qtdExperienciaPorAtaque;
    }
    
    public void sofrerDano() {
        if( this.podeSofrerDano() && qtdDano > 0.0 ) {
            //comparacao verdadeiro : falso            
            this.vida = this.vida >=  this.qtdDano ?
                this.vida - this.qtdDano : 0.0;           

            if ( this.vida == 0.0 ) {
                this.status = status.MORTO;
            }else {
                this.status = Status.SOFREU_DANO;
            }
        }
    }
    
    public abstract String imprimirResumo();  //obriga os filhos a implemantar esse metodo
    
}
