public class Dwarf extends Personagem {
    //parametros da classe
    
    //construtor da classe
    public Dwarf ( String nome ) {
        super(nome); // manda para a classe pai (personagem)
        //super.setNome(nome); chama o metodo set da classe pai
        this.vida = 110.0;
        this.inventario.adicionar(new Item(1, "Escudo"));
        this.qtdDano = 10.0;
    }    

    public void equiparEscudo() {
        this.qtdDano = 5.0;
    } 
    
    public String imprimirResumo() {
        return "Dwarf";
    }
    
}
