public class Elfo  extends Personagem {
    private int indiceFlecha;
    private static int qtdElfos;
    //private Status status; // nao precisa dar new porque nao tem construtor
        
    {
        indiceFlecha = 0;        
        //status = Status.RECEM_CRIADO; // nao precisa dar new porque nao tem construtor
    }
    
    //Construtor classe elfo
    public Elfo ( String nome ) {
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(2, "Flecha"));
        this.inventario.adicionar(new Item(1, "Arco"));
        Elfo.qtdElfos++;
    }           
    
    protected void finalize() throws Throwable { // throws tratamento de erros
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos() {
        return Elfo.qtdElfos;
    }
   
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
      
    // ternario this.getQtdFlecha() > 0 ? true : false;
    public boolean podeAtirarFlecha(){
        return this.getQtdFlecha() > 0;
    }
    
    public void atirarFlecha( Dwarf dwarf ) {
        if ( podeAtirarFlecha() ) {
        this.getFlecha().setQuantidade( this.getQtdFlecha() - 1);        
        //this.experiencia = experiencia + 1;
        aumentarXp();  
        dwarf.sofrerDano();
        this.sofrerDano();
        }
    }
    
    public String imprimirResumo() {
        return "Elfo";
    }
   
}