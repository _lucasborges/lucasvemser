import java.util.*;
public class EstatisticasInventario {
    Inventario inventario;
    //construtor da classe
  public EstatisticasInventario( Inventario inventario ) {
     this.inventario = inventario;
  }
    //metodos da classe
    public double calcularMedia() {
        if (this.inventario.getItens().isEmpty()) {
            return Double.NaN; // parametro nativo do double que diz que nao esta retornando nada.
        }
        double somaQtds = 0.0;
        for ( Item item : this.inventario.getItens() ) {
            somaQtds += item.getQuantidade();
        }
         return somaQtds / inventario.getItens().size();
        /*
        int soma = 0;
        for ( int i = 0; i < inventario.getItens().size(); i++ ) {
            //System.out.println("posicao "+i+""+inventario.obter(i).getQuantidade());
            soma += inventario.obter(i).getQuantidade();        
        }
        return soma / inventario.getItens().size();
        */
    }
   
    public double calcularMediana(){
      if ( this.inventario.getItens().isEmpty() ) {
          return Double.NaN;
      }
      
      int qtdItens = this.inventario.getItens().size();
      int meio = qtdItens / 2; // arredonda pra cima
      int qtdMeio = this.inventario.obter(meio).getQuantidade();
      if ( qtdItens % 2 == 1 ) {
          return qtdMeio;
      }
      
      int qtdMeioMenosUm = this.inventario.obter( meio - 1 ).getQuantidade();
      return ( qtdMeio + qtdMeioMenosUm ) / 2.0;
    
        /*
       double mediana=0.0;
       Inventario inventarioTemp;
       int[] totais=new int[inventario.getItens().size()];
       for ( int i = 0; i < inventario.getItens().size(); i++ ){
           totais[i]=inventario.obter(i).getQuantidade();
       }
       int temp=0;
       for ( int i = 0; i < inventario.getItens().size(); i++ ){
           for ( int j = i+1; j < inventario.getItens().size(); j++ ){
               if( totais[i] > totais[j] ){
                   temp=totais[i];
                   totais[i]=totais[j];
                   totais[j]=temp;
               }
            }
       }
       
       if( inventario.getItens().size()%2 == 0){
           mediana = (double)( totais[((inventario.getItens().size()/2)-1)] + totais[((inventario.getItens().size()/2)+1)-1] )/2;          
       }else{
           mediana = (double)totais[((inventario.getItens().size()/2))];
       }
       
       return mediana;
    */   
    }
    
   
    public int qtdItensAcimaDaMedia() {
      double media = this.calcularMedia();
      int qtdAcima = 0;
      
      for ( Item item : this.inventario.getItens() ) {
          if ( item.getQuantidade() > media ) {
              qtdAcima++;
          }
      } 
      
      return qtdAcima;
        
      /*
      int itensAcimaDaMedia = 0;  
      for ( int i = 0; i < inventario.getItens().size(); i++ ) {
          if ( inventario.obter(i).getQuantidade() > calcularMedia() ) {
                itensAcimaDaMedia += 1;
          }
      }
      return itensAcimaDaMedia;
      */
    }
   
   
}