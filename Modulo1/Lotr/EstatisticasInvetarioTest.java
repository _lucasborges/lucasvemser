import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class EstatisticasInvetarioTest  {    
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertTrue(Double.isNaN(estatisticas.calcularMedia()));
    }
    
    @Test
    public void calcularMediaComApenasUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(2, estatisticas.calcularMedia(), 1e-9);
    }
    
    @Test
    public void calcularMediaComDoisItens() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "Escudo"));
        inventario.adicionar(new Item(4, "Espada"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(3, estatisticas.calcularMedia(), 1e-9);
    }
    
    /*
    @Test
    public void mediaItensInventario() {
        Inventario inventario = new Inventario(2);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        Item arco = new Item(1,"Arco");
        Item flecha = new Item(1, "Flecha");
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        
        
        assertEquals(1, estatisticas.calcularMedia(), 1e-9);        
    }
    
    @Test
    public void medianaItensInventarioPar() {
        Inventario inventario = new Inventario(4);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        Item arco = new Item(2,"Arco");
        Item flecha = new Item(1, "Flecha");
        Item escudo = new Item(3, "Escudo");
        Item machado = new Item(6, "Machado");
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(machado);
        
        assertEquals(2.5, estatisticas.calcularMediana(), 1e-9);        
    }
   
    @Test
    public void medianaItensInventarioImpar() {
        Inventario inventario = new Inventario(3);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        Item arco = new Item(2,"Arco");
        Item flecha = new Item(1, "Flecha");
        Item escudo = new Item(3, "Escudo");
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        
        assertEquals(2.0, estatisticas.calcularMediana(), 1e-9);        
    }
    */
    @Test
    public void qtdItensAcimaDaMediaTest() {
        Inventario inventario = new Inventario(4);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        Item arco = new Item(2,"Arco");
        Item flecha = new Item(1, "Flecha");
        Item escudo = new Item(3, "Escudo");
        Item machado = new Item(6, "Machado");
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(machado);
        
        assertEquals(1, estatisticas.qtdItensAcimaDaMedia());        
    }
}