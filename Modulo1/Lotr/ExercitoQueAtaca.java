import java.util.*;
public class ExercitoQueAtaca extends ExercitoDeElfos {
  private Estrategias estrategia;
  
  public ExercitoQueAtaca( Estrategias estrategia ) {
      this.estrategia = estrategia;
  }
  
  public void trocarEstrategia( Estrategias estrategia ) {
      this.estrategia = estrategia;
  }
  
  public void atacar( ArrayList<Dwarf> dwarves ) {
      ArrayList<Elfo> ordem = this.estrategia.getOrdemDeAtaque( this.getexercitoDeElfos() );
      for ( Elfo elfo : ordem ) {
          for ( Dwarf dwarf : dwarves ) {
              elfo.atirarFlecha( dwarf );
          }
      }
  }
  
}
