import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest {
    @Test
    public void limitarItensInventario() {
        Inventario inventario = new Inventario(4);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        Item arco = new Item(2,"Arco");
        Item flecha = new Item(1, "Flecha");
        Item escudo = new Item(3, "Escudo");
        Item machado = new Item(6, "Machado");
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(machado);
        
        paginador.setPular(1);        
        assertEquals(flecha, paginador.limitar(2).get(0));
        assertEquals(escudo, paginador.limitar(2).get(1));
    }
}