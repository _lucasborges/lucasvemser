import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    
    @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXp() {
        Elfo novoElfo = new Elfo("Legolas"); 
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());         
    }

    @Test
    public void atirar2FlechasZerarFlechas2xAumentarXp() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());         
    }
    
    @Test
    public void atirar3FlechasZerarFlechas2xAumentarXp() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());         
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado() {
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void NaoCriaElfoNaoIncrementa() {
        assertEquals(0, Elfo.getQtdElfos());
    }
    
    @Test
    public void Cria1ElfoContadorUmaVez() {
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }
    
    @Test
    public void Cria2ElfoContadorDuasVezes() {
        Elfo novoElfo = new Elfo("Legolas");
        Elfo novoElfo1 = new Elfo("LegolasUm");
        assertEquals(2, Elfo.getQtdElfos());
    }
    
}