import java.util.*;
public class PaginadorInventario {
   Inventario inventario;
   
   private int marcador;
   
   {
       marcador = 0;
   }
   
   public PaginadorInventario( Inventario inventario ) {
        this.inventario = inventario;
    }
   
      
   public void setPular( int marcador ) {
       this.marcador = marcador;
   }
   
   public ArrayList<Item> limitar( int limitador ) {
       ArrayList<Item> paginador = new ArrayList<>();
       int fim = this.marcador + limitador;
       for ( int i = this.marcador; i < fim && i < this.inventario.getItens().size(); i++ ) {
           paginador.add(inventario.obter(i));
       }
       return paginador;
   }  
}