import java.util.*;
public class ElfoDaLuz extends Elfo {
    private int contadorDeAtaques;
    private final double QTD_VIDA_GANHA = 10.0;

    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
            Arrays.asList(
                "Espada de Galvorn"
            )
        );

    {
        contadorDeAtaques = 1;
    }

    public ElfoDaLuz ( String nome ) {
        super(nome);
        this.qtdDano = 21.0;
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));         
    }

    public boolean devePerderVida() {
        return contadorDeAtaques % 2 == 1;
    }

    @Override
    public void perderItem( Item item ) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if ( possoPerder ) {
            super.perderItem(item);
        } 
    }

    public void ganharVida() {
        this.vida += QTD_VIDA_GANHA;
    }

    public void atacarComEspada ( Dwarf dwarf ) {        
        if ( this.getStatus() != Status.MORTO ) {
            dwarf.sofrerDano();
            this.aumentarXp();
            if ( devePerderVida() ) {
                this.sofrerDano();
            } else {           
                this.ganharVida();          
            }
            contadorDeAtaques++;   
        }
    }
}
