import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    @Test
    public void ElfoDaLuzNasceComEpadaNoInventario() {
        ElfoDaLuz iluminado = new ElfoDaLuz("Iluminado");
        assertEquals("Espada de Galvorn", iluminado.getInventario().buscar("Espada de Galvorn").getDescricao());
    }
    
    @Test
    public void elfoDaLuzNaoPerdeEspadaDeGalvorn() {
        ElfoDaLuz iluminado = new ElfoDaLuz("Iluminado"); 
        Item espadaDeGalvorn = new Item(1,"Espada de Galvorn");
        iluminado.perderItem(espadaDeGalvorn);
        Inventario inventario = iluminado.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertEquals("Espada de Galvorn", inventario.buscar("Espada de Galvorn").getDescricao());
    }
    
    @Test
    public void ElfoDaLuzGanha1XpAoAtacarComEspada () {
        ElfoDaLuz iluminado = new ElfoDaLuz("Iluminado");
        iluminado.atacarComEspada(new Dwarf("Bigode"));
        assertEquals(1, iluminado.getExperiencia());
    }
    
    @Test
    public void ElfoDaLuzAtacaComEspadaEperde21 () {
        ElfoDaLuz iluminado = new ElfoDaLuz("Iluminado");
        iluminado.atacarComEspada(new Dwarf("Bigode"));
        assertEquals(79, iluminado.getVida(), 1e-9);
        assertEquals(Status.SOFREU_DANO, iluminado.getStatus());
    }
    
    @Test
    public void ElfoDaLuzAtacaComEspadaEperde21ERecupera10 () {
       ElfoDaLuz iluminado = new ElfoDaLuz("Iluminado");
       Dwarf bigode = new Dwarf("Bigode");
       iluminado.atacarComEspada(bigode);
       assertEquals(79, iluminado.getVida(), 1e-9);
       iluminado.atacarComEspada(bigode);
       assertEquals(89, iluminado.getVida(), 1e-9);
    }
    
    @Test
    public void ElfoDaLuzDevePerderVida() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        assertEquals(79, feanor.getVida(), 1e-9 );
        assertEquals(100, gul.getVida(), 1e-9 ); 
    }
    
    @Test
    public void elfoDaLuzDevePerderVidaEGanhar() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        feanor.atacarComEspada(gul);
        assertEquals(89, feanor.getVida(), 1e-9 );
        assertEquals(90, gul.getVida(), 1e-9 );
    }
}

