import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfosVerdesTest {
    @Test
    public void elfosVerdesGanha2XPorFlecha() {
        ElfosVerdes celebron = new ElfosVerdes("Celebron"); 
        celebron.atirarFlecha(new Dwarf ("Balin"));
        assertEquals(2, celebron.getExperiencia());        
    }
    
    @Test
    public void elfosVerdesAdicionarItemComDescricaoValida() {
        ElfosVerdes celebron = new ElfosVerdes("Celebron"); 
        Item arcoDeVidro = new Item (1, "Arco de vidro");
        celebron.ganharItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
    }
    
    @Test
    public void elfosVerdesAdicionarItemComDescricaoInvalida() {
        ElfosVerdes celebron = new ElfosVerdes("Celebron"); 
        Item arcoDeMadeira = new Item (1, "Arco de Madeira");
        celebron.ganharItem(arcoDeMadeira);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Madeira"));
    }
    
    @Test
    public void elfosVerdesPerderItemComDescricaoValida() {
        ElfosVerdes celebron = new ElfosVerdes("Celebron"); 
        Item arcoDeVidro = new Item (1, "Arco de vidro");
        celebron.ganharItem(arcoDeVidro);
        celebron.perderItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de vidro"));
    }
    
    @Test
    public void elfosVerdesPerderItemComDescricaoInvalida() {
        ElfosVerdes celebron = new ElfosVerdes("Celebron"); 
        Item arco = new Item (1, "Arco");
        celebron.ganharItem(arco);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
    }

    
}
