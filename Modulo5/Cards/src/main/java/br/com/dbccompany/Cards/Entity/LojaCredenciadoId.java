package br.com.dbccompany.Cards.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LojaCredenciadoId {
	
	@Column( name = "ID_LOJA" )
	private Integer idLoja;
	
	@Column( name = "ID_CRADENCIADOR" )
	private Integer idCredenciador;
	
	public LojaCredenciadoId() {		
	}
	
	public LojaCredenciadoId( Integer idLoja, Integer idCredenciador ) {
		this.idLoja = idLoja;
		this.idCredenciador = idCredenciador;
	}
}
