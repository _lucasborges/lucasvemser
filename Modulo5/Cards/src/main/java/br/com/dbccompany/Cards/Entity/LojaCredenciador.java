package br.com.dbccompany.Cards.Entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "LOJA" )
public class LojaCredenciador {
	
	@EmbeddedId
	private LojaCredenciadoId id;

	public LojaCredenciadoId getId() {
		return id;
	}

	public void setId(LojaCredenciadoId id) {
		this.id = id;
	}
	

	
}
