package br.com.dbccompany.Cards.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name = "LOJA" )

public class LojaEntity {
	@Id
	@SequenceGenerator( allocationSize = 1, name = "LOJA_SEQ", sequenceName = "LOJA_SEQ" )
	@GeneratedValue( generator = "LOJA_SEQ", strategy = GenerationType.SEQUENCE )
	
	@Column( name = "ID_LOJA", nullable = false )
	private Integer id;
	
	@ManyToMany( mappedBy = "lojas" )
	private List<LojaXCredenciador> lojas = new ArrayList<>();
	
	private String nome;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
