package br.com.dbccompany.Cards;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cards.Entity.BandeiraEntity;
import br.com.dbccompany.Cards.Entity.CartaoEntity;
import br.com.dbccompany.Cards.Entity.ClienteEntity;
import br.com.dbccompany.Cards.Entity.CredenciadorEntity;
import br.com.dbccompany.Cards.Entity.EmissorEntity;
import br.com.dbccompany.Cards.Entity.HibernateUtil;
import br.com.dbccompany.Cards.Entity.LancamentoEntity;
import br.com.dbccompany.Cards.Entity.LojaEntity;
import br.com.dbccompany.Cards.Entity.LojaXCredenciador;

public class Main {

	public static void main(String[] args) {		
			Session session = null;
			Transaction transaction = null;
			try {
				session = HibernateUtil.getSessionFactory().openSession();
				transaction = session.beginTransaction();
				
				LojaEntity loja = new LojaEntity();
				loja.setNome( "LOJA1" );
				List<LojaEntity> lojas = new ArrayList<>();
				lojas.add(loja);

				CredenciadorEntity credenciador = new CredenciadorEntity();
				credenciador.setNome( "CREDENCIADOR1" );
				List<CredenciadorEntity> credenciadores = new ArrayList<>();
				credenciadores.add( credenciador );
				
				LojaXCredenciador lojaXcredenciador = new LojaXCredenciador();
				lojaXcredenciador.setTaxa(0.1);
				lojaXcredenciador.setCredenciador( credenciadores );
				lojaXcredenciador.setLoja( lojas );
				
				lojaXcredenciador.setCredenciador(credenciadores);
				
				
				BandeiraEntity bandeira = new BandeiraEntity();
				bandeira.setNome( "BANDEIRA1" );
				bandeira.setTaxa(0.1);
				
				
				EmissorEntity emissor = new EmissorEntity();
				emissor.setNome( "EMISSOR1" );
				emissor.setTaxa( 0.1 );
				
				ClienteEntity cliente = new ClienteEntity();
				cliente.setNome( "CLIENTE1" );
				
				CartaoEntity cartao = new CartaoEntity();
				cartao.setChip( true );
				cartao.setVencimento(new Date());
				cartao.setBandeira( bandeira );
				cartao.setCliente( cliente );
				cartao.setEmissor( emissor );
				
				LancamentoEntity  lancamento = new LancamentoEntity();
				lancamento.setCartao( cartao );
				lancamento.setEmissor( emissor );
				lancamento.setLoja( loja );
				lancamento.setDescricao( "Pc gamer" );
				lancamento.setValor( 5000.0 );
				lancamento.setDataCompra(new Date());
				
				session.save( loja );
				session.save( credenciador );
				session.save( bandeira );
				session.save( emissor );
				session.save( cartao );
				session.save( cliente );
				session.save( lojaXcredenciador );
				session.save( lancamento );
				
				
				transaction.commit();
			}
			catch ( Exception e ) {
				if ( transaction != null ) {
					transaction.rollback();
				}
				System.exit(1);
			}finally {
				System.exit(0);
			}	
	}

}
