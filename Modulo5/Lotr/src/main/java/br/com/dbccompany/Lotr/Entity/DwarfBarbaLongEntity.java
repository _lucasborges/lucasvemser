package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "DWARF_BARBA_LONGA" )
public class DwarfBarbaLongEntity extends DwarfEntity {
	
	public DwarfBarbaLongEntity() {
		super.setTipo(Tipo.DWARF_BARBA_LONGA);
	}
}
