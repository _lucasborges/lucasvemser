package br.com.dbccompany.Lotr.Entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name = "ITENS" )
public class ItemEntity {
		
		@Id
		@SequenceGenerator( allocationSize = 1, name = "ITEM_SEQ", sequenceName = "ITEM_SEQ" )
		@GeneratedValue( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE )
		@Column( name = "ID_ITEM", nullable = false )
		private Integer id;
		
		private String descricao;
		
		@ManyToMany( mappedBy = "itens" )
		private List<InventarioXItem> inventarios = new ArrayList<>();
		
		public List<InventarioXItem> getInventarios() {
			return inventarios;
		}

		public void setInventarios(List<InventarioXItem> inventarios) {
			this.inventarios = inventarios;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}			
	
}

