package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table ( name = "ESTADOS" )
public class Estado {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ" )
    @GeneratedValue( generator = "ESTADO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_ESTADO", nullable = false )
    private Integer id;

    private String nome;

    public Estado(){
    }

    public Estado( Integer id, String nome ){
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @ManyToOne ( cascade = CascadeType.ALL)
    @JoinColumn ( name = "PAISES_ID" )
    private Pais pais;

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    @OneToMany( mappedBy = "estado")
    private List<Cidade> cidades;

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

}
