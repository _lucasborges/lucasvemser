package br.com.dbccompany.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.Pais;
import br.com.dbccompany.bancoDigital.Service.BancoService;
import br.com.dbccompany.bancoDigital.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pais" )
public class PaisController {

    @Autowired
    PaisService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Pais> todosPaises(){
        return service.todosPaises();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Pais novoPais( @RequestBody Pais pais ){
        return service.salvar( pais );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pais editarPais( @PathVariable Integer id, @RequestBody Pais pais ){
        return service.editar( pais, id );
    }

    @GetMapping( value = "/{nome}" )
    @ResponseBody
    public Pais buscarPorNomePais(@PathVariable String nome ){
        return service.findByNome( nome );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Pais buscarPorIdBanco(@PathVariable Integer id ){
        return service.paisEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
