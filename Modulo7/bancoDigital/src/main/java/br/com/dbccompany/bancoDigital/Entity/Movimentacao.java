package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "MOVIMENTACOES" )
public class Movimentacao {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ" )
    @GeneratedValue( generator = ",MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_MOVIMENTACAO", nullable = false )
    private Integer id;

    private TipoMovimentacao tipoMovimentacao;
    private Double valor;

    public Movimentacao(){
    }

    public Movimentacao( Integer id, TipoMovimentacao tipoMovimentacao, Double valor ){
        this.id = id;
        this.tipoMovimentacao = tipoMovimentacao;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoMovimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @OneToMany( mappedBy = "movimentacao")
    private List<Conta> contas;

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }
}
