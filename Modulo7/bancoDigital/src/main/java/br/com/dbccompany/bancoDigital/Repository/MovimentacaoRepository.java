package br.com.dbccompany.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.Entity.Movimentacao;
import br.com.dbccompany.bancoDigital.Entity.TipoMovimentacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Integer> {

    Movimentacao findByTipoMovimentacao(TipoMovimentacao movimentacao);
    Movimentacao findByValor( Double valor );
    List<Movimentacao> findAll();
}
