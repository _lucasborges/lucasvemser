package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CONTAS")
public class Conta {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ" )
    @GeneratedValue( generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_CONTA", nullable = false )
    private Integer id;

    private Integer numero;
    private Double saldo;

    public Conta(){
    }

    public Conta( Integer id, Integer numero, Double saldo ){
        this.id = id;
        this.numero = numero;
        this.saldo = saldo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "Agencia_ID" )
    private Agencia agencia;

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "TIPOS_CONTA_ID" )
    private TiposConta tiposConta;

    public TiposConta getTiposConta() {
        return tiposConta;
    }

    public void setTiposConta(TiposConta tiposConta) {
        this.tiposConta = tiposConta;
    }

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "MOVIMENTACAO_ID" )
    private Movimentacao movimentacao;

    public Movimentacao getMovimentacao() {
        return movimentacao;
    }

    public void setMovimentacao(Movimentacao movimentacao) {
        this.movimentacao = movimentacao;
    }

    @ManyToMany
    @JoinTable(
            name = "CLIENTE_CONTA",
            joinColumns = @JoinColumn(name = "CONTA_ID"),
            inverseJoinColumns = @JoinColumn(name = "CLIENTE_ID")
    )
    private List<Cliente> clientes = new ArrayList<>();

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
