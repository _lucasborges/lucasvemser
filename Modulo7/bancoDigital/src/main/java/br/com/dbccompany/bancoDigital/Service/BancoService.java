package br.com.dbccompany.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.Cliente;
import br.com.dbccompany.bancoDigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService {

    @Autowired
    private BancoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Banco salvar( Banco banco ){
        return repository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class )
    public Banco editar( Banco banco, Integer id ){
        banco.setId( id );
        return repository.save( banco );
    }

    public List<Banco> todosBancos() {
        return repository.findAll();
    }

    public Banco bancoEspecifico( Integer id ){
        Optional<Banco> banco = repository.findById( id );
        return banco.get();
    }

    public Banco findByNome(String nome ){
        return repository.findByNome( nome );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }

}
