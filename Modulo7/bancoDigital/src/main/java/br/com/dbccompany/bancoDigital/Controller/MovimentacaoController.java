package br.com.dbccompany.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.Entity.Cidade;
import br.com.dbccompany.bancoDigital.Entity.Movimentacao;
import br.com.dbccompany.bancoDigital.Entity.TipoMovimentacao;
import br.com.dbccompany.bancoDigital.Service.CidadeService;
import br.com.dbccompany.bancoDigital.Service.MovimentacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/movimentacao" )
public class MovimentacaoController {

    @Autowired
    MovimentacaoService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Movimentacao> todasMovimentacoes(){
        return service.todasMovimentacoes();
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Movimentacao novaMovimentacao( @RequestBody Movimentacao movimentacao ){
        return service.salvar( movimentacao );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Movimentacao editarMovimentacao( @PathVariable Integer id, @RequestBody Movimentacao movimentacao ){
        return service.editar( movimentacao, id );
    }

    @GetMapping( value = "/{tipo}" )
    @ResponseBody
    public Movimentacao buscarPorTipoMovimentacao( @PathVariable TipoMovimentacao tipo ){
        return service.findByTipoMovimentacao( tipo );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Movimentacao buscarPorIdMovimentacao( @PathVariable Integer id ){
        return service.movimentacaoEspecifica( id );
    }

    @GetMapping( value = "/{valor}" )
    @ResponseBody
    public Movimentacao buscarPorValorMovimentacao( @PathVariable Double valor ){
        return service.findByValor( valor );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
