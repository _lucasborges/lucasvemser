package br.com.dbccompany.bancoDigital.Entity;

public enum TipoMovimentacao {
    DEBITO,
    CREDITO
}
