package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CIDADES")
public class Cidade {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ" )
    @GeneratedValue( generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_CIDADE", nullable = false )
    private Integer id;

    private String nome;

    public Cidade(){
    }

    public Cidade(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @ManyToOne ( cascade = CascadeType.ALL)
    @JoinColumn ( name = "ESTADO_ID" )
    private Estado estado;

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @OneToMany( mappedBy = "cidade")
    private List<Agencia> agencias;

    public List<Agencia> getAgencias() {
        return agencias;
    }

    public void setAgencias(List<Agencia> agencias) {
        this.agencias = agencias;
    }

    @ManyToMany
    @JoinTable(
            name = "CIDADE_CLIENTE",
            joinColumns = @JoinColumn(name = "CIDADE_ID"),
            inverseJoinColumns = @JoinColumn(name = "CLIENTE_ID")
    )
    private List<Cliente> clientes = new ArrayList<>();

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
