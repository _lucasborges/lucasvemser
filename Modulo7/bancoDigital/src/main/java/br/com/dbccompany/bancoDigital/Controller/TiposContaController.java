package br.com.dbccompany.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.TiposConta;
import br.com.dbccompany.bancoDigital.Service.BancoService;
import br.com.dbccompany.bancoDigital.Service.TiposContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/tiposconta" )
public class TiposContaController {

    @Autowired
    TiposContaService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TiposConta> todosTiposContas(){
        return service.todosTiposContas();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TiposConta novoTiposConta( @RequestBody TiposConta tiposConta ){
        return service.salvar( tiposConta );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TiposConta editarTiposConta( @PathVariable Integer id, @RequestBody TiposConta tiposConta ){
        return service.editar( tiposConta, id );
    }

    @GetMapping( value = "/{descricao}" )
    @ResponseBody
    public TiposConta buscarPorDescricaoTiposConta(@PathVariable String descricao ){
        return service.findByDescricao( descricao );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public TiposConta buscarPorIdTiposConta(@PathVariable Integer id ){
        return service.tiposContaEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }
}
