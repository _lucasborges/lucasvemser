package br.com.dbccompany.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.Cliente;
import br.com.dbccompany.bancoDigital.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/banco" )
public class BancoController {

    @Autowired
    BancoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Banco> todosBancos(){
        return service.todosBancos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Banco novoBanco( @RequestBody Banco banco ){
        return service.salvar( banco );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Banco editarBanco( @PathVariable Integer id, @RequestBody Banco banco ){
        return service.editar( banco, id );
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Banco buscarPorNomeBanco(@PathVariable String nome ){
        return service.findByNome( nome );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Banco buscarPorIdBanco(@PathVariable Integer id ){
        return service.bancoEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
