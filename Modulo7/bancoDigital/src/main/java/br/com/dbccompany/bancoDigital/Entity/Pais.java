package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table ( name = "PAISES" )
public class Pais {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "PAIS_SEQ", sequenceName = "PAIS_SEQ" )
    @GeneratedValue( generator = "PAIS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_PAIS", nullable = false )
    private Integer id;

    private String nome;

    public Pais(){
    }

    public Pais( Integer id, String nome ) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @OneToMany( mappedBy = "pais")
    private List<Estado> estados;

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }
}
