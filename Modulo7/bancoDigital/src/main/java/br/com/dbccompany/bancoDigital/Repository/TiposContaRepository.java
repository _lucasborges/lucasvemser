package br.com.dbccompany.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.TiposConta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TiposContaRepository extends CrudRepository<TiposConta, Integer> {

    TiposConta findByDescricao( String descricao );
    List<TiposConta> findAll();
}
