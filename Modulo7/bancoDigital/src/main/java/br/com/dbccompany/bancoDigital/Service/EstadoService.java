package br.com.dbccompany.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.Cliente;
import br.com.dbccompany.bancoDigital.Entity.Estado;
import br.com.dbccompany.bancoDigital.Repository.BancoRepository;
import br.com.dbccompany.bancoDigital.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoService {
    @Autowired
    private EstadoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Estado salvar(Estado estado ){
        return repository.save(estado);
    }

    @Transactional( rollbackFor = Exception.class )
    public Estado editar(Estado estado, Integer id ){
        estado.setId( id );
        return repository.save( estado );
    }

    public List<Estado> todosEstados() {
        return repository.findAll();
    }

    public Estado estadoEspecifico( Integer id ){
        Optional<Estado> estado = repository.findById( id );
        return estado.get();
    }

    public Estado findByNome(String nome ){
        return repository.findByNome( nome );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }

}
