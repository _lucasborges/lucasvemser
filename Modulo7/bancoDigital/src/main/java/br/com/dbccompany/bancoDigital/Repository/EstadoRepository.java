package br.com.dbccompany.bancoDigital.Repository;


import br.com.dbccompany.bancoDigital.Entity.Estado;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EstadoRepository extends CrudRepository<Estado, Integer> {
    Estado findByNome(String nome );
    List<Estado> findAll();
}
