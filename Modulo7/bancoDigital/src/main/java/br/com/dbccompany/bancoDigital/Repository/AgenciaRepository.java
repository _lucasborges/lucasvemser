package br.com.dbccompany.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.Entity.Agencia;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgenciaRepository extends CrudRepository <Agencia, Integer> {

    Agencia findByNome( String nome );
    List<Agencia> findAll();
}
