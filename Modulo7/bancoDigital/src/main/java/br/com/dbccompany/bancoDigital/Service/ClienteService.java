package br.com.dbccompany.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.Entity.Cliente;
import br.com.dbccompany.bancoDigital.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Cliente salvar(Cliente cliente ){
        return repository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public Cliente editar( Cliente cliente, Integer id ){
        cliente.setId( id );
        return repository.save( cliente );
    }

    public List<Cliente> todosClientes() {
        return repository.findAll();
    }

    public Cliente clienteEspecifico( Integer id ){
        Optional<Cliente> cliente = repository.findById( id );
        return cliente.get();
    }

    public Cliente findByNome( String nome ){
        return repository.findByNome( nome );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }

}
