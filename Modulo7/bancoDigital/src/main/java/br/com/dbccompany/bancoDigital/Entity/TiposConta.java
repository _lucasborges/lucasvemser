package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "TIPOS_CONTAS")
public class TiposConta {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "TIPOS_CONTA_SEQ", sequenceName = "TIPOS_CONTA_SEQ" )
    @GeneratedValue( generator = "TIPOS_CONTA_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_TIPOS_CONTA", nullable = false )
    private Integer id;

    private String descricao;

    public TiposConta(){
    }

    public TiposConta( Integer id, String descricao ){
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @OneToMany( mappedBy = "tiposConta")
    private List<Conta> contas;

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }
}
