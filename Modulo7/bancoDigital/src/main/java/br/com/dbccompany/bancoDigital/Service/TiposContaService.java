package br.com.dbccompany.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.Entity.TiposConta;
import br.com.dbccompany.bancoDigital.Repository.TiposContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TiposContaService {

    @Autowired
    private TiposContaRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public TiposConta salvar(TiposConta tiposConta ){
        return repository.save(tiposConta);
    }

    @Transactional( rollbackFor = Exception.class )
    public TiposConta editar( TiposConta tiposConta, Integer id ){
        tiposConta.setId( id );
        return repository.save( tiposConta );
    }

    public List<TiposConta> todosTiposContas() {
        return repository.findAll();
    }

    public TiposConta tiposContaEspecifico( Integer id ){
        Optional<TiposConta> tiposConta = repository.findById( id );
        return tiposConta.get();
    }

    public TiposConta findByDescricao( String descricao ){
        return repository.findByDescricao( descricao );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }
}
