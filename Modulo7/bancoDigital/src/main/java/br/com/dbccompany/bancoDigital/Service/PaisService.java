package br.com.dbccompany.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.Entity.Cliente;
import br.com.dbccompany.bancoDigital.Entity.Pais;
import br.com.dbccompany.bancoDigital.Repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PaisService {

    @Autowired
    private PaisRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Pais salvar(Pais pais ){
        return repository.save(pais);
    }

    @Transactional( rollbackFor = Exception.class )
    public Pais editar( Pais pais, Integer id ){
        pais.setId( id );
        return repository.save( pais );
    }

    public List<Pais> todosPaises() {
        return repository.findAll();
    }

    public Pais paisEspecifico( Integer id ){
        Optional<Pais> pais = repository.findById( id );
        return pais.get();
    }

    public Pais findByNome(String nome ){
        return repository.findByNome( nome );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }

}
