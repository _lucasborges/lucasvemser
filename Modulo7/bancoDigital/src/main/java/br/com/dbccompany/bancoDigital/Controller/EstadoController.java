package br.com.dbccompany.bancoDigital.Controller;


import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.Estado;
import br.com.dbccompany.bancoDigital.Service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/estado" )
public class EstadoController {

    @Autowired
    EstadoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Estado> todosEstados(){
        return service.todosEstados();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Estado novoEstado( @RequestBody Estado estado ){
        return service.salvar( estado );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Estado editarEstado( @PathVariable Integer id, @RequestBody Estado estado ){
        return service.editar( estado, id );
    }

    @GetMapping( value = "/{nome}" )
    @ResponseBody
    public Estado buscarPorNomeEstado(@PathVariable String nome ){
        return service.findByNome( nome );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Estado buscarPorIdEstado(@PathVariable Integer id ){
        return service.estadoEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
