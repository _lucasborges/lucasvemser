package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "AGENCIAS")
public class Agencia {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ" )
    @GeneratedValue( generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_AGENCIA", nullable = false )
    private Integer id;
    private String nome;

    public Agencia(){
    }

    public Agencia( Integer id, String nome ){
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "BANCO_ID" )
    private Banco banco;

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "CIDADE_ID" )
    private Cidade cidade;

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    @OneToMany( mappedBy = "agencia")
    private List<Conta> contas = new ArrayList<>();

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }
}
