package br.com.dbccompany.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

    Cliente findByNome(String nome );
    Cliente findByCpf( String cpf );
    List<Cliente> findAll();
}
