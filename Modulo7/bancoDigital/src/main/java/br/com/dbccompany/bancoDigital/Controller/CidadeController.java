package br.com.dbccompany.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.Entity.Cidade;
import br.com.dbccompany.bancoDigital.Service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/cidade" )
public class CidadeController {

    @Autowired
    CidadeService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Cidade> todasCidades(){
        return service.todasCidades();
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Cidade novaCidade( @RequestBody Cidade cidade ){
        return service.salvar( cidade );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Cidade editarCidade( @PathVariable Integer id, @RequestBody Cidade cidade ){
        return service.editar( cidade, id );
    }

    @GetMapping( value = "/{nome}" )
    @ResponseBody
    public Cidade buscarPorNomeCidade(@PathVariable String nome ){
        return service.findByNome( nome );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Cidade buscarPorIdCidade(@PathVariable Integer id ){
        return service.cidadeEspecifica( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
