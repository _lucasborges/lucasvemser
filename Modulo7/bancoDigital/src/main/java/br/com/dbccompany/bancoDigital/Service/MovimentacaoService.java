package br.com.dbccompany.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.Entity.Movimentacao;
import br.com.dbccompany.bancoDigital.Entity.TipoMovimentacao;
import br.com.dbccompany.bancoDigital.Repository.MovimentacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class MovimentacaoService {

    @Autowired
    private MovimentacaoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Movimentacao salvar(Movimentacao movimentacao ){
        return repository.save( movimentacao );
    }

    @Transactional( rollbackFor = Exception.class )
    public Movimentacao editar( Movimentacao movimentacao, Integer id ){
        movimentacao.setId( id );
        return repository.save( movimentacao );
    }

    public List<Movimentacao> todasMovimentacoes() {
        return repository.findAll();
    }

    public Movimentacao movimentacaoEspecifica( Integer id ){
        Optional<Movimentacao> movimentacao = repository.findById( id );
        return movimentacao.get();
    }

    public Movimentacao findByTipoMovimentacao( TipoMovimentacao tipoMovimentacao ){
        Optional<Movimentacao> movimentacao = Optional.ofNullable(repository.findByTipoMovimentacao(tipoMovimentacao));
        return movimentacao.get();
    }

    public Movimentacao findByValor( Double valor ){
        return repository.findByValor( valor );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }

}
