package br.com.dbccompany.bancoDigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "BANCOS")
public class Banco {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "BANCOS_SEQ", sequenceName = "BANCOS_SEQ" )
    @GeneratedValue( generator = "BANCOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_BANCO", nullable = false )
    private Integer id;

    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @OneToMany( mappedBy = "banco")
    private List<Agencia> agencias;

    public List<Agencia> getAgencias() {
        return agencias;
    }

    public void setAgencias(List<Agencia> agencias) {
        this.agencias = agencias;
    }
}
