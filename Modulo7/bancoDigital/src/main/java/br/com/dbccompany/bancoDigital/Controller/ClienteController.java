package br.com.dbccompany.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.Cliente;
import br.com.dbccompany.bancoDigital.Service.BancoService;
import br.com.dbccompany.bancoDigital.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/cliente" )
public class ClienteController {

    @Autowired
    ClienteService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Cliente> todosClientes(){
        return service.todosClientes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Cliente novoCliente( @RequestBody Cliente cliente ){
        return service.salvar( cliente );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Cliente editarCliente( @PathVariable Integer id, @RequestBody Cliente cliente ){
        return service.editar( cliente, id );
    }

    @GetMapping( value = "/{nome}" )
    @ResponseBody
    public Cliente buscarPorNomeCliente( @PathVariable String nome ){
        return service.findByNome( nome );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Cliente buscarPorIdCliente(@PathVariable Integer id ){
        return service.clienteEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
