package br.com.dbccompany.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.Entity.Pais;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaisRepository extends CrudRepository<Pais, Integer > {
    Pais findByNome( String nome );
    List<Pais> findAll();
}
