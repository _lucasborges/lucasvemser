package br.com.dbccompany.coworking.Entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TipoContatoTest {

    @Test
    public void testGetNome() {
        TipoContato tipoContato1 = new TipoContato();
        tipoContato1.setNome("email");

        Assertions.assertEquals("email", tipoContato1.getNome());
    }
}