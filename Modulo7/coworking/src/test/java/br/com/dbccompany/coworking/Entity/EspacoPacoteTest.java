package br.com.dbccompany.coworking.Entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EspacoPacoteTest {

    @Test
    public void testGetEspaco() {
        EspacoPacote espacoPacote1 = new EspacoPacote();
        Espaco espaco1 = new Espaco();
        espaco1.setId(1);
        espacoPacote1.setEspaco(espaco1);

        Assertions.assertEquals( 1 ,espacoPacote1.getEspaco().getId());
    }

    @Test
    public void testGetPacote() {
        EspacoPacote espacoPacote1 = new EspacoPacote();
        Pacote pacote1 = new Pacote();
        pacote1.setId(13);
        espacoPacote1.setPacote(pacote1);

        Assertions.assertEquals( 13 ,espacoPacote1.getPacote().getId());
    }

    @Test
    public void testGetTipoContratacao() {
        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setTipoContratacao(TipoContratacao.HORA);

        Assertions.assertEquals(TipoContratacao.HORA, espacoPacote1.getTipoContratacao());
    }

    @Test
    public void testGetQuantidade() {
        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setQuantidade(3);

        Assertions.assertEquals(3, espacoPacote1.getQuantidade());
    }

    @Test
    public void testGetPrazo() {
        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setPrazo(10);

        Assertions.assertEquals(10, espacoPacote1.getPrazo());
    }
}