package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Security.Criptografar;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class UsuarioTest {

    @Test
    public void testGetNome() {
        Usuario usuario1 = new Usuario();
        usuario1.setNome("lucas");

        Assertions.assertEquals("lucas", usuario1.getNome());
    }

    @Test
    void getEmail() {
        Usuario usuario1 = new Usuario();
        usuario1.setEmail("lucas@gmail");

        Assertions.assertEquals("lucas@gmail", usuario1.getEmail());
    }

    @Test
    void getLogin() {
        Usuario usuario1 = new Usuario();
        usuario1.setLogin("lucas22");

        Assertions.assertEquals("lucas22", usuario1.getLogin());
    }

    @Test
    void getSenha() {
        Usuario usuario1 = new Usuario();
        usuario1.setSenha("12345000");
        Usuario usuario2 = new Usuario();
        usuario2.setSenha("12345000");


        Assertions.assertEquals("12345000", usuario1.getSenha());
        Assertions.assertEquals("12345000", usuario2.getSenha());
    }
}