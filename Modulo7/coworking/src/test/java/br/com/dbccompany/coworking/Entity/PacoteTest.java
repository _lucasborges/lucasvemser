package br.com.dbccompany.coworking.Entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PacoteTest {

    @Test
    public void testGetValor() {
        Pacote pacote1 = new Pacote();
        pacote1.setValor(200.0);

        Assertions.assertEquals(200.0, pacote1.getValor());
    }
}