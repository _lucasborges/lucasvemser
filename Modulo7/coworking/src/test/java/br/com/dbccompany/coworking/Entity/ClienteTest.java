package br.com.dbccompany.coworking.Entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


class ClienteTest {

    @Test
    public void testGetNomeEqual() {
        Cliente cliente1 = new Cliente();
        cliente1.setNome("Lucas");

        Assertions.assertEquals( "Lucas", cliente1.getNome() );
    }

    @Test
    public void testGetCpf() {
        Cliente cliente1 = new Cliente();
        cliente1.setCpf( "000.000.000-01" );

        Assertions.assertEquals( "000.000.000-01", cliente1.getCpf() );
    }

    @Test
    public void testGetDataNascimento() {
        Cliente cliente1 = new Cliente();
        cliente1.setDataNascimento(new Date());

        Assertions.assertEquals( new Date(), cliente1.getDataNascimento() );
    }


}