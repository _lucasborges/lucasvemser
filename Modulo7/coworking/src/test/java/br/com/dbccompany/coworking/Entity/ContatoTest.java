package br.com.dbccompany.coworking.Entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ContatoTest {

    @Test
    public void testGetTipoContato() {
        Contato contato1 = new Contato();
        TipoContato tipoContato1 = new TipoContato();
        tipoContato1.setNome("email");
        contato1.setTipoContato(tipoContato1);

        Assertions.assertEquals("email", contato1.getTipoContato().getNome());
    }

    @Test
    public void testGetValor() {
        Contato contato1 = new Contato();
        contato1.setValor("lucas@gmail");

        Assertions.assertEquals("lucas@gmail", contato1.getValor());
    }
}