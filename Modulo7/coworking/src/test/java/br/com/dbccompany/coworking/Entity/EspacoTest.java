package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EspacoTest {

    @Test
    public void testGetNome() {
        Espaco espaco1 = new Espaco();
        espaco1.setNome("cozinha");

        Assertions.assertEquals("cozinha", espaco1.getNome());
    }

    @Test
    void getQtdPessoas() {
        Espaco espaco1 = new Espaco();
        espaco1.setQtdPessoas(10);

        Assertions.assertEquals(10, espaco1.getQtdPessoas());
    }

    @Test
    void getValor() {
        Espaco espaco1 = new Espaco();
        espaco1.setValor(150.50);

        Assertions.assertEquals(150.50, espaco1.getValor());
    }
}