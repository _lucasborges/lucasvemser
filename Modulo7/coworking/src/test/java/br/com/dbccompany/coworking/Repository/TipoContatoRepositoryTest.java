package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContato;
import org.junit.After;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


import static org.junit.jupiter.api.Assertions.*;
class TipoContatoRepositoryTest {

    @After
    public void tearDown() {
        System.gc();
    }

    @Autowired
    TipoContatoRepository repo;

    @Test
   public void findByNome() {
        TipoContato tipoContato1 = new TipoContato();
        TipoContato tipoContato2 = new TipoContato();
        tipoContato1.setNome("email");
        repo.save(tipoContato1);
        tipoContato2.setNome("telefone");
        repo.save(tipoContato2);

        Assertions.assertEquals("email", repo.findByNome("email"));
        Assertions.assertEquals("telefone", repo.findByNome("telefone"));
    }

//    @Test
//    void findAll() {
//        TipoContato tipoContato1 = new TipoContato();
//        TipoContato tipoContato2 = new TipoContato();
//        tipoContato1.setNome("email");
//        repo.save(tipoContato1);
//        tipoContato2.setNome("fone");
//        repo.save(tipoContato2);
//
//        Assertions.assertEquals("email, fone", repo.findAll());
//    }
}