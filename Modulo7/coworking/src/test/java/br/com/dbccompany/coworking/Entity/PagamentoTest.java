package br.com.dbccompany.coworking.Entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PagamentoTest {

    @Test
    public void testGetClientePacote() {
        Pagamento pagamento1 = new Pagamento();
        ClientePacote clientePacote1 = new ClientePacote();
        clientePacote1.setId(1);
        pagamento1.setClientePacote( clientePacote1 );

        Assertions.assertEquals(1, pagamento1.getClientePacote().getId());
    }

    @Test
    public void testGetContratacao() {
        Contratacao contratacao1 = new Contratacao();
        Pagamento pagamento1 = new Pagamento();
        contratacao1.setTipoContratacao(TipoContratacao.SEMANA);
        pagamento1.setContratacao(contratacao1);

        Assertions.assertEquals(TipoContratacao.SEMANA, pagamento1.getContratacao().getTipoContratacao());
    }

    @Test
    public void TestGetTipoPagamento() {
        Pagamento pagamento1 = new Pagamento();
        pagamento1.setTipoPagamento(TipoPagamento.DEBITO);

        Assertions.assertEquals(TipoPagamento.DEBITO, pagamento1.getTipoPagamento());
    }
}