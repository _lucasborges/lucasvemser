package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Espaco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoRepository extends CrudRepository<Espaco, Integer> {

    Espaco findByNome( String nome );
    Espaco findByQtdPessoas( Integer qtdPessoas );
    Espaco findByValor( Double valor );
    List<Espaco> findAll();
}
