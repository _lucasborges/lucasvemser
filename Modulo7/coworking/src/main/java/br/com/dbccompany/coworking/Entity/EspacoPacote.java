package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "ESPACOS_X_PACOTES")
public class EspacoPacote {

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue( generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_ESPACO_PACOTE" )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "espaco_id")
    private Espaco espaco;

    @ManyToOne
    @JoinColumn( name = "pacote_id" )
    private Pacote pacote;

    private TipoContratacao tipoContratacao;

    private Integer quantidade;
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
