package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/tipocontato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TipoContato> todosContatos(){
        return service.todosTipoContato();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoContato novoTipoContato( @RequestBody TipoContato tipoContato ){
        return service.salvar( tipoContato );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContato editarTipoContato( @PathVariable Integer id, @RequestBody TipoContato tipoContato ){
        return service.editar( tipoContato, id );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public TipoContato buscarPorIdTipoContato(@PathVariable Integer id ){
        return service.tipoContatoEspecifico( id );
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public TipoContato buscarPorNomeTipoContato(@PathVariable String nome ){
        return service.findByNome( nome );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
