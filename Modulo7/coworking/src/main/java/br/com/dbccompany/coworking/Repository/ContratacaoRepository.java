package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer> {

    Contratacao findByTipoContratacao( TipoContratacao tipoContratacao );
    Contratacao findByQuantidade( Integer quantidade );
    Contratacao findByDesconto( Double desconto );
    Contratacao findByPrazo( Integer prazo );
    List<Contratacao> findAll();
}
