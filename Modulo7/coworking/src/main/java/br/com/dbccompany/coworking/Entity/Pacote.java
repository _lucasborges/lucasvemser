package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "PACOTES" )
public class Pacote {

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue( generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_PACOTE" )
    private Integer id;

    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }


}
