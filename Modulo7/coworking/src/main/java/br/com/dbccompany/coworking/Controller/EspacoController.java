package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/espaco")
public class EspacoController {

    @Autowired
    EspacoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Espaco> todosEspacos() {
        return service.todosEspacos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Espaco novoEspaco(@RequestBody Espaco espaco){
        return service.salvar( espaco );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Espaco editarEspaco( @PathVariable Integer id, @RequestBody Espaco espaco ){
        return service.editar( espaco, id );
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Espaco buscarPorIdEspaco( @PathVariable Integer id ){
        return service.espacoEspecifico( id );
    }

    @GetMapping( value = "/nome/{nome}")
    @ResponseBody
    public Espaco buscarPorNomeEspaco( @PathVariable String nome ){
        return service.findByNome( nome );
    }

    @GetMapping( value = "/quantidade/{qtd}")
    @ResponseBody
    public Espaco buscarPorEspacoQuantidade( @PathVariable Integer quantidade ){
        return service.findByQtdPessoas( quantidade );
    }

    @GetMapping( value = "/valor/{valor}")
    @ResponseBody
    public Espaco buscarPorEspacoValor( @PathVariable Double valor ){
        return service.findByValor( valor );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
