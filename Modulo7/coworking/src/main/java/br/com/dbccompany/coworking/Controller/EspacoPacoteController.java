package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Entity.EspacoPacote;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/espacopacote")
public class EspacoPacoteController {

    @Autowired
    EspacoPacoteService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<EspacoPacote> todosEspacoPacote() {
        return service.todosEspacoPacote();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public EspacoPacote novoEspacoPacote(@RequestBody EspacoPacote espacoPacote ){
        return service.salvar( espacoPacote );
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacoPacote editarEspacoPacote( @PathVariable Integer id, @RequestBody EspacoPacote espacoPacote ){
        return service.editarEspacoPacote( espacoPacote, id );
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public EspacoPacote buscarPorIdEspacoPacote( @PathVariable Integer id ){
        return service.espacoPacoteEspecifico( id );
    }

    @GetMapping( value = "/tipoespacopacote/{tipo}")
    @ResponseBody
    public EspacoPacote buscarPorTipoEspacoPacote(@PathVariable TipoContratacao tipoContratacao){
        return service.findByTipoEspacoPacote( tipoContratacao );
    }

    @GetMapping( value = "/quantidade/{qtd}")
    @ResponseBody
    public EspacoPacote buscarPorEspacoPacoteQuantidade(@PathVariable Integer quantidade ){
        return service.findByQuantidade( quantidade );
    }

    @GetMapping( value = "/prazo/{prazo}")
    @ResponseBody
    public EspacoPacote buscarPorPrazoEspacoPacote( @PathVariable Integer prazo ){
        return service.findByPrazo( prazo );
    }



}
