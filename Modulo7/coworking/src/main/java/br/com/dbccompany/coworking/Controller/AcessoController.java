package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acesso;
import br.com.dbccompany.coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping( value = "api/acesso")
public class AcessoController {

    @Autowired
    AcessoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Acesso> todosAcessos(){
        return service.todosAcessos();
    }

    @PostMapping( value = "novo")
    @ResponseBody
    public Acesso novoAcesso(@RequestBody Acesso acesso ){
        return service.salvar( acesso );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Acesso buscaPorIdAcesso( @PathVariable Integer id ){
        return service.acessoEspecifico( id );
    }

    @GetMapping( value = "/data/{data}")
    @ResponseBody
    public Acesso buscaPorDataAcesso( @PathVariable Date date ){
        return service.findByDate( date );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
