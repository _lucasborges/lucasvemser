package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Cliente;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Cliente salvar( Cliente cliente ){
        return  repository.save( cliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public Cliente editar( Cliente cliente, Integer id ){
        cliente.setId( id );
        return repository.save( cliente );
    }

    public List<Cliente> todosClientes(){
        return (List<Cliente>) repository.findAll();
    }

    public Cliente clienteEspecifico( Integer id ){
        Optional<Cliente> cliente = repository.findById( id);
        return cliente.get();
    }

    public Cliente findByNome( String nome ){
        return repository.findByNome( nome );
    }

    public Cliente findByCpf( String cpf ){
        return repository.findByCpf( cpf );
    }

    public Cliente findByDataNascimento( Date date ){
        return repository.findByDataNascimento( date );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }

}
