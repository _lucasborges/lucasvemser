package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Usuario;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Usuario> todosBancos(){
        return service.todosUsuarios();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Usuario novoUsuario( @RequestBody Usuario usuario ){
        return service.salvar( usuario );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Usuario editarUsuario( @PathVariable Integer id, @RequestBody Usuario usuario ){
        return service.editarUsuario( usuario, id );
    }

    @GetMapping( value = "/email/{email}" )
    @ResponseBody
    public Usuario buscarPorEmailUsuario(@PathVariable String email ){
        return service.findByEmail( email );
    }

    @GetMapping( value = "/login/{login}" )
    @ResponseBody
    public Usuario buscarPorLoginUsuario(@PathVariable String login ){
        return service.findByLogin( login );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Usuario buscarPorIdUsuario(@PathVariable Integer id ){
        return service.usuarioEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}



