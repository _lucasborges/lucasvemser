package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTATOS" )
public class Contato {

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_CONTATO" )
    private Integer id;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "tipocontato_id" )
    private TipoContato tipoContato;

    private String valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
