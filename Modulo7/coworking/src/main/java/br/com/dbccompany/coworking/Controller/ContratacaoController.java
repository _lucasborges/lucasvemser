package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Contratacao> todasContratacoes() {
        return service.todasContratacoes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Contratacao novaContratacao(@RequestBody Contratacao contratacao ){
        return service.salvar( contratacao );
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Contratacao editarContratacao( @PathVariable Integer id, @RequestBody Contratacao contratacao ){
        return service.editar( contratacao, id );
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Contratacao buscarPorIdContratacao( @PathVariable Integer id ){
        return service.contratacaoEspecifica( id );
    }

    @GetMapping( value = "/tipo/{tipocontratacao}")
    @ResponseBody
    public Contratacao buscarPorTipoContratacao(@PathVariable TipoContratacao tipoContratacao){
        return service.findByTipoContratacao( tipoContratacao );
    }

    @GetMapping( value = "/prazo/{prazo}")
    @ResponseBody
    public Contratacao buscarPorPrazo(@PathVariable Integer prazo ){
        return service.findByPrazo( prazo );
    }

    @GetMapping( value = "/quantidade/{qtd}")
    @ResponseBody
    public Contratacao buscarPorQuantidade(@PathVariable Integer quantidade ){
        return service.findByQuantidade( quantidade );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }
}
