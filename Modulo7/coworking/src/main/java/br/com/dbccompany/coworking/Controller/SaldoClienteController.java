package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping( value = "/api/saldocliente")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<SaldoCliente> todosSaldoClientes(){
        return service.todosSaldosClientes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public SaldoCliente novoSaldoCliente(@RequestBody SaldoCliente saldoCliente ){
        return service.salvar( saldoCliente );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public SaldoCliente editarSaldoCliente(@PathVariable SaldoClienteId id, @RequestBody SaldoCliente saldoCliente ){
        return service.editar( saldoCliente, id );
    }

    @GetMapping( value = "/tipocontratacao/{tipo}" )
    @ResponseBody
    public SaldoCliente buscarPorTipoContratacaoSaldoCliente(@PathVariable TipoContratacao tipoContratacao){
        return service.findByTipoContratacao( tipoContratacao );
    }

    @GetMapping( value = "/quantidade/{qtd}" )
    @ResponseBody
    public SaldoCliente buscarPorQuantidadeSaldoCliente(@PathVariable Integer quantidade ){
        return service.findByQuatidade( quantidade );
    }

    @GetMapping( value = "/vencimento/{venc}" )
    @ResponseBody
    public SaldoCliente buscarPorVencimentoSaldoCliente(@PathVariable Date vencimento ){
        return service.findByVencimento( vencimento );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public SaldoCliente buscarPorIdSaldoCliente(@PathVariable Integer id ){
        return service.saldoClienteEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }
}
