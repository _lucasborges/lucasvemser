package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pagamento;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Pagamento salvar(Pagamento pagamento ) {
        return repository.save( pagamento );
    }

    @Transactional( rollbackFor = Exception.class)
    public Pagamento editar( Pagamento pagamento, Integer id ){
        pagamento.setId( id );
        return repository.save( pagamento );
    }

    public List<Pagamento> todosClientesPacotes(){
        return repository.findAll();
    }

    public Pagamento pagamentoEspecifico( Integer id ){
        Optional<Pagamento> pagamento = repository.findById( id );
        return pagamento.get();
    }

    public Pagamento findByTipoPagamento( TipoPagamento tipoPagamento ){
        return (Pagamento) repository.findByTipoPagamento( tipoPagamento );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }

}
