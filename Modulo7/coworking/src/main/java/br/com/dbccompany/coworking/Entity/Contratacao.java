package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTRATACAO")
public class Contratacao {

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_CONTRATACAO" )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "espaco_id" )
    private Espaco espaco;

    @ManyToOne
    @JoinColumn( name = "cliente_id" )
    private Cliente cliente;

    private TipoContratacao tipoContratacao;

    private Integer quantidade;

    private Double desconto;

    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco( Espaco espaco ) {
        this.espaco = espaco;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente( Cliente cliente ) {
        this.cliente = cliente;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao( TipoContratacao tipoContratacao ) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto( Double desconto ) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo( Integer prazo ) {
        this.prazo = prazo;
    }

}
