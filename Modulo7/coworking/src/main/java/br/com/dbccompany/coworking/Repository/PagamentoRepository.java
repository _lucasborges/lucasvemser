package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pagamento;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    Pagamento findByTipoPagamento( TipoPagamento tipoPagamento );
    List<Pagamento> findAll();
}
