package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacote;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pacote" )
public class PacoteController {

    @Autowired
    PacoteService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Pacote> todosPacote() {
        return service.todosPacotes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Pacote novoPacote(@RequestBody Pacote pacote ){
        return service.salvar( pacote );
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Pacote editarPacote( @PathVariable Integer id, @RequestBody Pacote pacote ){
        return service.editar( pacote, id );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Pacote buscaPorIdPacote( @PathVariable Integer id ){
        return service.pacoteEspecifico( id );
    }

    @GetMapping( value = "/valor/{valor}" )
    @ResponseBody
    public Pacote buscaPorValorPacote( @PathVariable Double valor ){
        return service.findByValor( valor );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

 }
