package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pagamento;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import br.com.dbccompany.coworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Pagamento> todosPagamentos(){
        return service.todosClientesPacotes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Pagamento novoPagamento( @RequestBody Pagamento pagamento ){
        return service.salvar( pagamento );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pagamento editarPagamento( @PathVariable Integer id, @RequestBody Pagamento pagamento ){
        return service.editar( pagamento, id );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Pagamento buscarPorIdPagamento(@PathVariable Integer id ){
        return service.pagamentoEspecifico( id );
    }

    @GetMapping( value = "/tipopagamento/{tipo}")
    @ResponseBody
    public Pagamento buscarPorTipoPagamento(@PathVariable TipoPagamento tipoPagamento){
        return service.findByTipoPagamento( tipoPagamento );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }
}
