package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "ACESSOS")
public class Acesso {

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_ACESSO", nullable = false )
    private Integer id;

    @ManyToOne( cascade = CascadeType.MERGE )
    @MapsId( "cliente_id" )
    @JoinColumn( name = "cliente_saldo_cliente_id")
    private Cliente cliente;

    @ManyToOne( cascade = CascadeType.MERGE)
    @MapsId( "espaco_id" )
    @JoinColumn( name = "espaco_saldo_cliente_id")
    private Espaco espaco;

    private Boolean isEntrada;

    private Date data;

    private Boolean isExcecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void Espaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }
}
