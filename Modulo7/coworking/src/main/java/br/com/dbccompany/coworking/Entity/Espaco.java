package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table( name = "ESPACOS")
public class Espaco {

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue( generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_ESPACO", nullable = false)
    private Integer id;

    @Column( unique = true, nullable = false)
    private String nome;

    @Column( nullable = false)
    private Integer qtdPessoas;

    @Column( nullable = false)
    private Double valor;

    @OneToMany( mappedBy = "espaco")
    Set<SaldoCliente> saldoClientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Set<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(Set<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }

}
