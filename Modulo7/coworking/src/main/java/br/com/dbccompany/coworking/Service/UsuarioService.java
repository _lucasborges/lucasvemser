package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Usuario;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import br.com.dbccompany.coworking.Security.Criptografar;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Usuario salvar(Usuario usuario ){
        String senha = usuario.getSenha();
        if( senha.length() >= 6 && StringUtils.isAlphanumeric(senha) ){
            usuario.setSenha(Criptografar.criptografar(senha));
        }else {
            throw new RuntimeException("Senha deve ter 6 digitos alfanumericos.");
        }
        return repository.save(usuario);
    }

    @Transactional( rollbackFor = Exception.class )
    public Usuario editarUsuario( Usuario usuario, Integer id ){
        usuario.setId( id );
        return repository.save( usuario );
    }

    public List<Usuario> todosUsuarios(){
        return repository.findAll();
    }

    public Usuario usuarioEspecifico(Integer id ){
        Optional<Usuario> usuario = repository.findById( id);
        return usuario.get();
    }

    public Usuario findByNome( String nome ){
        return repository.findByNome( nome );
    }

    public Usuario findByEmail( String email ){
        return repository.findByEmail( email );
    }

    public Usuario findByLogin( String login ){
        return repository.findByLogin( login );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }
}
