package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Espaco salvar( Espaco espaco ){
        return repository.save( espaco );
    }

    @Transactional( rollbackFor = Exception.class )
    public Espaco editar( Espaco espaco, Integer id ){
        espaco.setId( id );
        return repository.save( espaco );
    }

    public List<Espaco> todosEspacos(){
        return repository.findAll();
    }

    public Espaco espacoEspecifico( Integer id ){
        Optional<Espaco> espaco = repository.findById( id );
        return espaco.get();
    }

    public Espaco findByNome( String nome ){
        return repository.findByNome( nome );
    }

    public Espaco findByQtdPessoas( Integer quantidade ){
        return repository.findByQtdPessoas( quantidade );
    }

    public Espaco findByValor( Double valor ){
        return repository.findByValor( valor );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }
}
