package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Contato salvar(Contato contato ){
        return repository.save( contato );
    }

    @Transactional( rollbackFor = Exception.class)
    public Contato editar( Contato contato, Integer id ){
        contato.setId( id );
        return repository.save( contato );
    }

    public List<Contato> todosContatos(){
        return repository.findAll();
    }

    public Contato contatoEspecifico( Integer id ){
        Optional<Contato> contato = repository.findById( id );
        return contato.get();
    }

    public Contato findByValor( String valor ){
        return repository.findByValor( valor );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }


}
