package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Currency;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

@Entity
@Table( name = "CLIENTES")
public class Cliente {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_CLIENTE", nullable = false )
    private Integer id;

    @Column( nullable = false )
    private String nome;

    @Column( nullable = false, unique = true, length = 14)
    private String cpf;

    @Column( nullable = false )
    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy" )
    private Date dataNascimento;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "contato_id", nullable = false)
    private Contato contato;

    @OneToMany( mappedBy = "cliente")
    Set<SaldoCliente> saldos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento( Date dataNascimento ) {
        this.dataNascimento = dataNascimento;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Set<SaldoCliente> getSaldos() {
        return saldos;
    }

    public void setSaldos(Set<SaldoCliente> saldos) {
        this.saldos = saldos;
    }
}
