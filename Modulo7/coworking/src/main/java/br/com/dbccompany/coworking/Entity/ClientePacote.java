package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CLIENTES_X_PACOTES")
public class ClientePacote {

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "CLIENTE_PACOTE_SEQ", sequenceName = "CLIENTE_PACOTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_CLIENTE_PACOTE" )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "cliente_id")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn( name = "pacote_id")
    private Pacote pacote;

    private Integer quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
