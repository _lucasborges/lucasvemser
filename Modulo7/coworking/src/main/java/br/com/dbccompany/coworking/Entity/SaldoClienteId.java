package br.com.dbccompany.coworking.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {

    @Column( name = "cliente_id")
    private Integer clienteId;

    @Column( name = "espaco_id")
    private Integer espacoId;

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer espacoId) {
        this.espacoId = espacoId;
    }

}
