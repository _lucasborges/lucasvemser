package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "PAGAMENTOS")
public class Pagamento {

    @Id
    @SequenceGenerator( allocationSize = 1, name =  "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue( generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_PAGAMENTO" )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "cliente_pacote_id" )
    private ClientePacote clientePacote;

    @ManyToOne
    @JoinColumn( name = "contratacao_id" )
    private Contratacao contratacao;

    private TipoPagamento tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacote getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacote clientePacote) {
        this.clientePacote = clientePacote;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

}
