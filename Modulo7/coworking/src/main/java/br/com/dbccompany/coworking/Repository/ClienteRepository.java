package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

    Cliente findByNome( String nome );
    Cliente findByCpf( String cpf );
    Cliente findByDataNascimento( Date date );
}
