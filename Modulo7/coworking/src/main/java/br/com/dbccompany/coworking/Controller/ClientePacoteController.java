package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientePacote;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/clientepacote")
public class ClientePacoteController {

    @Autowired
    ClientePacoteService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ClientePacote> todosClientesPacotes() {
        return service.todosClientesPacotes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientePacote novoClientePacote(@RequestBody ClientePacote clientePacote){
        return service.salvar( clientePacote );
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ClientePacote editarClientePacote( @PathVariable Integer id, @RequestBody ClientePacote clientePacote ){
        return service.editar( clientePacote, id );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ClientePacote buscarPorIdBanco(@PathVariable Integer id ){
        return service.clientePacoteEspecifico( id );
    }

    @GetMapping( value = "/quantidade/{qtd}")
    @ResponseBody
    public ClientePacote buscaPorClientePacoteQtd( @PathVariable Integer quantidade ){
        return service.findByQuantidade( quantidade );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }

}
