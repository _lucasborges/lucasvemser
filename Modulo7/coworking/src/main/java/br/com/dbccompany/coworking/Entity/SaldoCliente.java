package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "SALDO_X_CLIENTE")
public class SaldoCliente {

    @EmbeddedId
    private SaldoClienteId id;

    @ManyToOne
    @MapsId( "cliente_id" )
    @JoinColumn( name = "cliente_id")
    private Cliente cliente;

    @ManyToOne
    @MapsId( "espaco_id" )
    @JoinColumn( name = "espaco_id")
    private Espaco espaco;

    private TipoContratacao tipoContratacao;
    private Integer quantidade;
    private Date vencimento;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
