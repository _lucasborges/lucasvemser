function cardapioIFood( veggie = true, comLactose = true ) {
    const cardapio = [
    'enroladinho de salsicha',
    'cuca de uva',
    'pastel de carne'
    ]

    let cardapioPersonalizado = cardapio;    



    if ( comLactose ) {
      cardapioPersonalizado.splice( 2, 0, 'pastel de queijo' );
    }

    if ( veggie ) {
      cardapioPersonalizado.splice( 0, 1 );
      cardapioPersonalizado.splice( 2, 1 );
      cardapioPersonalizado.splice( 2, 0, 'empada de legumes marabijosa');
    }
    
    let resultadoFinal = []
    let i = 0;
    while (i <= cardapioPersonalizado.length - 1) {
      resultadoFinal[i] = cardapioPersonalizado[i].toUpperCase()
      i++
    }
    
    return resultadoFinal;
  }
  console.log(cardapioIFood( )); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

  /*marcos 
    spread cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa'];
    let resultado = cardapio
                    .filter(alimento => alimento.length > 12)// uma condição para filtrar no array
                    .map(alimento => alimento.toUpperCase());//map é usando para criacao de outro array

    function cardapioIFood( veggie = true, comLactose = true ) {
    let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva',
    ]

    if ( comLactose ) {
      cardapio.push( 'pastel de queijo' );
    }

    cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa'];

    if ( veggie ) {
      cardapioPersonalizado.splice( cardapio.indexOf('enroladinho de salsicha'), 1 );
      cardapioPersonalizado.splice( cardapio.indexOf('pastel de carne'), 1);
    }
    
    let resultadoFinal = cardapio.map(alimento => alimento.toUpperCase());
        
    return resultadoFinal;
  }
  console.log(cardapioIFood( )); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]



  */