/*
let vemSer = {
    local: 'DBC',
    ano: '2020',
    imprimirInformacoes: function (quantidade, modulo) {
        return console.log(`Argumentos: ${this.local} | ${this.ano} | ${quantidade} | ${modulo}`);
    }
}

console.log(vemSer.imprimirInformacoes(20, 3));
*/

// criando classe 

class Jedi {
    constructor(nome) {
        this.nome = nome
    }

    atacarComSabre(){
        setTimeout(() => {
            console.log(`${ this.nome } atacou com sabre!`)
        }, 1000);
    }

    atacarComSabreSelf(){
        let self = this
        setTimeout(function() {
            console.log(`${ self.nome } atacou com sabre 2!`)
        },1000);
    }

}

let luke = new Jedi("Luke");
console.log(luke);
luke.atacarComSabre();
luke.atacarComSabreSelf();


// promise

let defer = new Promise((resolve, reject) => {
    setTimeout(() => {
        if (true) {
            resolve('Foi resolvido!');
        }else {
          reject ('Erro!');
        }
    }, 2000);
});

defer
    .then((data) => console.log(data)) // then trata o resultado de uma promesa.
    .catch((erro) => console.log(erro));

    //tratamento encadeado
/*
defer   
    .then((data) => {
        console.log(data)
        return "Novo resultado"
    })
    .then((data) => console.log(data))
    .catch((erro) => console.log(erro));        */

    let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/800`);

    pokemon 
        .then( data => data.json() )
        .then( data => console.log(data.name) );

