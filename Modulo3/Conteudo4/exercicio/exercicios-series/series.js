var series = JSON.parse('[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]')
console.log(series);

Array.prototype.invalidas = function invalidas() {
    //const res = "series inválidas: "
    const inv = this.filter(serie => serie.anoEstreia > 2020 || serie.titulo == null || serie.diretor == null 
                            || serie.genero == null || serie.elenco == null || serie.temporadas == null 
                            || serie.numeroEpisodios == null || serie.distribuidora == null );
                         
    return inv;
}
console.log (series.invalidas());


Array.prototype.filtarPorAno = function filtarPorAno( ano ) {
    const porAno = this.filter(serie => serie.anoEstreia >= ano);
    return porAno;
}
console.log (series.filtarPorAno(2017));


Array.prototype.procurarPorNome = function procurarPorNome( nome ) {
    const porNome = this.some( serie => serie.elenco.includes( nome ) );
    return porNome;
}
console.log(series.procurarPorNome("Damian Lewis"));


Array.prototype.mediaDeEp = function mediaDeEp() {
    const totalSeries = this.length;
    const totalEp = (this.map((serie) => serie.numeroEpisodios).reduce((total, eps) => total + eps));
    return totalEp / totalSeries;
}
console.log(series.mediaDeEp());

Array.prototype.totalSalarios = function totalSalarios( indice ) {
   const diretores = parseInt(this[indice].diretor.length) * 100000.00;
   const atores = parseInt(this[indice].elenco.length) * 40000.00;
   const res = `${this[indice].titulo} - total de salários: R$ ${atores + diretores},00 `;
   return res;
}
console.log(series.totalSalarios( 3 ));

Array.prototype.queroGenero = function queroGenero( genero ) {
    const gen = this.filter( serie => serie.genero.includes( genero ) );
    return gen;
}
console.log(series.queroGenero( "Drama" ));

