const pokeApi = new PokeApi();
const busca = document.getElementById( 'btn-buscar' );
const sorte = document.getElementById( 'btn-sorte' );
const search = document.getElementById( 'busca' );
const numeros = [];

function renderizacaoPokemon( pokemon ) {
  const telaPokedex = document.getElementById( 'tela' );
  const corpoPokedex = document.getElementById( 'card-body' );
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const imgPokemon = telaPokedex.querySelector( '.card-img-top' );
  const nome = corpoPokedex.querySelector( '#nome' );
  const id = dadosPokemon.querySelector( '#id' );
  const peso = dadosPokemon.querySelector( '#peso' );
  const altura = dadosPokemon.querySelector( '#altura' );
  const tipo = dadosPokemon.querySelector( '#tipo' );
  const estatPokemon = dadosPokemon.querySelector( '#estat' );

  imgPokemon.src = pokemon.imgPokemon;
  nome.innerHTML = pokemon.nome;
  id.innerHTML = `Id: ${ pokemon.id }`;
  peso.innerHTML = `Peso: ${ pokemon.pesoPokemon / 10 } Kg`;
  altura.innerHTML = `Altura: ${ pokemon.alturaPokemon / 10 } cm`;
  tipo.innerHTML = `Estilo: ${ pokemon.tipoPokemon }`;
  estatPokemon.innerHTML = `Estatisticas: ${ pokemon.estatPokemon.map( ( element ) => element.stat.name + element.base_stat ) }`;
}

async function buscar( id ) {
  if ( search.value >= 1 && search.value <= 807 ) {
    const pokemonEspecifico = await pokeApi.buscar( id );
    const poke = new Pokemon( pokemonEspecifico );
    renderizacaoPokemon( poke );
  } else {
    console.log( 'error' );
  }
}

console.log( numeros );

busca.addEventListener( 'click', () => {
  buscar( search.value );
} );

function pickRandom() {
  const aleatorio = Math.floor( Math.random() * 802 );
  if ( numeros.includes( aleatorio ) ) {
    console.log( 'error' );
  } else {
    numeros.push( aleatorio );
    return aleatorio;
  }
  return null;
}


sorte.addEventListener( 'click', () => {
  pickRandom();
  buscar( pickRandom() );
} );

buscar();
