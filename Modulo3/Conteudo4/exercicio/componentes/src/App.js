import React, { Component } from 'react';
import './App.css';

//import CompA, { CompB } from './componentes/ExemploComponenteBasico'
import Membros from './componentes/Membros';

class App extends Component {
  /* constructor( props ) {
    super( props );  
} */

render() {
  return (
    <div className="App">
      <Membros nome="João" sobrenome="Silva" />
      <Membros nome="Maria" sobrenome="Silva" />
      <Membros nome="Pedro" sobrenome="Silva" />
      {/* <CompA/>
      <CompB/>
      <CompA/>
      <CompA/>
      <CompA/>
      <CompA/> */}
    </div>
  );
}
}


export default App;
