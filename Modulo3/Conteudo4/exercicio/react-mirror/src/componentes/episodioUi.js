import React, { Component } from 'react';


export default class EpisodioUi extends Component {
    render() {
        const { episodio } = this.props
        return ( 
            <React.Fragment>
                <h2>{episodio.nome}</h2>
                <img src={episodio.url} alt={episodio.nome}></img>
                <span>{episodio.assistido ? 'Sim' : 'Não'}, { episodio.qtdVezesAssistido } vez(es) </span>
                <span>Nota: {episodio.nota}</span>
                <span>Duração: {episodio.duracaoEmMinutos} minutos</span>
                <span>Temporada / Episódio: {episodio.temporadaEpisodio}</span>        
            </React.Fragment>
        ) 
    }
}