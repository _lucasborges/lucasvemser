import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Mirror from './Mirror';
import InfoJsflix from './InfoJsFlix';
import ListaDeAvaliacao from './componentes/ListaDeAvaliacao';
import TelaDetalheEpisodio from './componentes/TelaDetalheEpisodio';


class App extends Component {
  

  render() {
    
    return (      
      <Router>
        <Route path="/mirror" component={ Mirror }/>
        <Route path="/infojsflix" component={ InfoJsflix } /> 
        <Route path="/" exact component={ Home }/> 
        <Route path="/avaliacoes" component={ ListaDeAvaliacao } />  
        <Route path="/episodio/:id" component={ TelaDetalheEpisodio } />     
      </Router>
    );
  }
}


const Home = () => 
  <section>
    <div>      
      <Link className='button button-blue' to="/mirror">React Mirror</Link>
      <Link className='button button-green' to="/infojsflix">Info JsFlix</Link>
    </div>
  </section>

export default App;
