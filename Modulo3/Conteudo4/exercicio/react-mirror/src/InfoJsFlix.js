import React, { Component } from 'react';
import ListaSeries from './ListaSeries';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class Infos extends Component {
  constructor(props) {
    super(props)
    this.ListaSeries = new ListaSeries();
    this.state = {
        texto: [],
        array:false,
        valor: null
    }
  }
  valor(evt){    this.setState({
        valor: evt.target.value
    })
  }
  filtrarAno(){
    this.setState({
        texto: []
      })
      const {valor} = this.state
      const teste = this.ListaSeries.filtrarPorAno(valor)
      this.setState({
        texto: teste,
        array: true
      })
  }
  invalidas(){
    this.setState({
        texto: []
      })
    const teste = this.ListaSeries.invalidas();
    this.setState({
        texto: teste,
        array: false
      })
  } 
   procuraPorNome(){
    this.setState({
        texto: []
      })
    const {valor} = this.state
    const teste = this.ListaSeries.procuraPorNome(valor).toString();
    this.setState({
        texto: teste,
        array: false
      })
  }  
  mediaDeEpisodios(){
    this.setState({
        texto: []
      })
    const teste = this.ListaSeries.mediaDeEpisodios().toString();
    this.setState({
        texto: teste,
        array: false
      })
  }  
  totalSalarios(){
    this.setState({
        texto: []
      })
    const {valor} = this.state
    if(valor !== null && valor !== ''){
    const teste = this.ListaSeries.totalSalarios(valor).toString();
    this.setState({
        texto: teste,
        array: false
      })
    }else{
      const teste = this.ListaSeries.totalSalarios(0).toString();
    this.setState({
        texto: teste,
        array: false
      })
    }
  }
  queroGenero(){
    this.setState({
        texto: []
      })
      const {valor} = this.state
      const teste = this.ListaSeries.queroGenero(valor)
      this.setState({
        texto: teste,
        array: true
      })
  }  
  queroTitulo(){
    this.setState({
        texto: []
      })
      const {valor} = this.state
      if(valor != null){
      const teste = this.ListaSeries.queroTitulo(valor)
      this.setState({
        texto: teste,
        array: true
      })
    }
  } 
   geraArray(){
    const {texto} = this.state 
    return(
          <div>
            { this.state.array && (            <div>
            { texto.map( (serie) => {
                return <p>{ serie.titulo }</p>
            } ) }
            </div>
          )
        }
        </div>
    )
  }
  geraString(){
    const {texto} = this.state 
    console.log(texto)
    return(
          <div>
            { !this.state.array && (            <div>
            <p>{texto}</p>
            </div>
          )
        }
        </div>
    )
  }  render() {
    return (
      <div className="App App-header">
          {this.geraString()}
          {this.geraArray()}
          <input type="text" placeholder="Digite o ano" onBlur={this.valor.bind(this)}/>
      <div>        
          <button className="button button-green" onClick={this.filtrarAno.bind(this)}>Filtrar por ano</button>
          <button className="button button-green" onClick={this.invalidas.bind(this)}>SeriesInvalidas</button>
          <button className="button button-green" onClick={this.procuraPorNome.bind(this)}>Procura por Nome</button>
          <button className="button button-green" onClick={this.mediaDeEpisodios.bind(this)}>Media de episodios</button>
          <button className="button button-green" onClick={this.totalSalarios.bind(this)}>Total de salários</button>
          <button className="button button-green" onClick={this.queroGenero.bind(this)}>Procura por genero</button>
          <button className="button button-green" onClick={this.queroTitulo.bind(this)}>Procura por titulo</button>          
      </div>
      <div>      
        <Link className='button button-blue' to="/mirror">React Mirror</Link>
        <Link className='button button-green' to="/infojsflix">Info JsFlix</Link>
      </div>
      </div>
    );
  }
}export default Infos;