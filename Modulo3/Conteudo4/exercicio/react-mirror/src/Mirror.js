import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
//import logo from './logo.svg';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './componentes/episodioUi';
import MensagemFlash from './componentes/MensagemFlash';
import MeuInputNumero from './componentes/MeuInputNumero';

class Home extends Component {
  constructor(props) {
    super(props)
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this ) chamar this.sortear
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      cor: "verde",
      mensagem: '',
      deveExibirErro: false
    }
  }
  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }


  assistido() {
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido(episodio)
    episodio.marcarComoAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota({ nota, erro }) {
    this.setState({
      deveExibirErro:erro
    })
    if (erro){
      return;
    }
    
    
    let cor, mensagem
    const { episodio } = this.state
    if ( episodio.validarNota( nota ) ){
      episodio.avaliar(nota) 
      cor = 'verde'
      mensagem = 'Registramos sua nota!' 
      
    } else {
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida entre 1 e 5!'

    }
    this.exibirMensagem({ cor, mensagem })
      
  }

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })  
  }

  /* geraCampoDeNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <input type="text" placeholder="Insira a nota do EP" id="nota" onBlur={ this.registrarNota.bind(this) }></input>
              <span id="mensagem"></span>
            </div>
          )
        }
      </div>
    )
  } */

  atualizarMensagem = devoExibir => {
    console.log( devoExibir )
    this.setState({
      exibirMensagem: devoExibir
    })
  }    

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state
    const { listaEpisodios } = this
    //this.state.episodio
    return (      
      <div className="App">
        {/* { deveExibirMensagem ? (<span>Registramos sua nota!!!</span>) : '' } */}
        <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                       deveExibirMensagem={ exibirMensagem }
                       mensagem={ mensagem }
                       cor={ cor }
                       segundos={ 2 }
                        />                                   
        <header className='App-header'>
        <div>      
          <Link className='button button-blue' to="/mirror">React Mirror</Link>
          <Link className='button button-green' to="/infojsflix">Info JsFlix</Link>
        </div>
            <EpisodioUi episodio = { episodio } />            
          <div>
            <button onClick={this.sortear.bind(this)}>Próximo</button>
            <button onClick={this.assistido.bind(this)}>Já Assisti</button>            
            <Link className="button button-green" to ={{ pathname: "/avaliacoes", state: {listaEpisodios} }}>Avaliações</Link>
          </div>
        {/* { this.geraCampoDeNota() } */}
        <MeuInputNumero  placeholder="1 a 5"
                         mensagemCampo="Qual sua nota para esse episódio?"
                         visivel={ episodio.assistido || false }
                         obrigatorio={ true }
                         atualizarValor={ this.registrarNota.bind(this) }
                         deveExibirErro={ deveExibirErro }
        />
        </header>
      </div>
    );
  }
}

export default Home;
