import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import './App.css';

import './layout-css/grid.css';
import './layout-css/reset.css';
import ComponenteIndex from './ComponenteIndex';
import ComponenteServico from './ComponenteServico';
import ComponenteSobreNos from './ComponenteSobreNos';
import ComponenteContato from './ComponenteContato';


class App extends Component {
  render() {
    return (
          <Router>
            <Route path="/" exact component={ ComponenteIndex } />
            <Route path="/Servicos" component={ ComponenteServico } />
            <Route path="/Sobre-nos" component={ ComponenteSobreNos } />
            <Route path="/Contato" component={ ComponenteContato } />
          </Router>      
        );
      }
    }
    
    export default App;

