import React, { Component } from 'react';

import Artigo from '../Artigo-componente/Artigo'
import Button from '../Button-componente/Buttons';
import './banner.css';

export default class Banner extends Component {
    render () {
        return (
            <section className="main-banner">
            <article>
            <Artigo titulo={<h1>Título</h1>} />
                <Button class={'button button-outline button-blue '} href={'#'} text={'Saiba Mais'}/>
            </article>
        </section>
        )
    }
}