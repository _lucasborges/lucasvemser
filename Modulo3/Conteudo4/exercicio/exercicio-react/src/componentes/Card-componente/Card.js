import React from 'react';

import Button from '../Button-componente/Buttons';
import './box.css';
import Artigo from '../Artigo-componente/Artigo'

export default ( props ) =>
    <div class={ props.class }>
        <article className="box">
            <div>
                <img src={ props.imagem } alt={ props.alt }/>
            </div>
            <Artigo titulo={<h4>Título</h4>} />
                <Button class={ 'button button-green' } href={'#'} text={'Saiba Mais'}/>
        </article>
    </div>         