import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import './footer.css';

export default class Footer extends Component {
    render() {
        return (
            <footer class="main-footer">
                <div class="container">
                    <nav>
                        <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/Sobre-nos">Sobre nós</Link>
                        </li>
                        <li>
                            <Link to="/Servicos">Serviços</Link>
                        </li>
                        <li>
                            <Link to="/Contato">Contato</Link>
                        </li>
                        </ul>
                    </nav>
                    <p>
                        &copy; Copyright DBC Company - 2019
                    </p>
                </div>
            </footer>
        )
    }
    
    
}