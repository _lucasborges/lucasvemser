import React, { Component } from 'react';

import Artigo from '../Artigo-componente/Artigo'; 
import './sobreNos.css';

export default ( props ) => 
    <React.Fragment>        
            <div class="row">
                <article class={ props.classe }>
                    <img src={ props.imagem } alt={ props.atl } />
                    <Artigo titulo={<h2>Título</h2>} />
                </article>
            </div>
    </React.Fragment>
