import React from 'react';

import './buttons.css';

export default ( props ) =>
            <React.Fragment>
                <a className={ props.class } href={ props.href }>{props.text}</a>
            </React.Fragment>
       
