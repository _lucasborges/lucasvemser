import React, { Component } from 'react';

import Header from './componentes/Header-componente/Header';
import Footer from './componentes/Footer-componente/Footer';
import SobreNos from './componentes/SobreNos-componente/SobreNos';

import imagemNos from './componentes/SobreNos-componente/imagemSobreNos.jpg';

export default class ComponenteSobreNos extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <section className='container about-us'>
                    <SobreNos className='col-paddingtop' classe={ 'col col-12 col-paddingtop' } imagem={ imagemNos } alt={ imagemNos } />
                    <SobreNos classe={ 'col col-12' } imagem={ imagemNos } alt={ imagemNos } />
                    <SobreNos classe={ 'col col-12' } imagem={ imagemNos } alt={ imagemNos } />
                    <SobreNos classe={ 'col col-12' } imagem={ imagemNos } alt={ imagemNos } />
                </section>
                <Footer />
            </React.Fragment>
        )
    }
}