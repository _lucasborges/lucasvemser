import React, { Component } from 'react';

import imagem1 from './componentes/Card-componente/imagem1.png';
import imagem2 from './componentes/Card-componente/imagem2.png';
import Card from './componentes/Card-componente/Card';
import Header from './componentes/Header-componente/Header';
import Footer from './componentes/Footer-componente/Footer';
import Artigo from './componentes/Artigo-componente/Artigo';
import Button from './componentes/Button-componente/Buttons';

export default class ComponenteServico extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <section className='container'>
                    <div className='row'>
                        <Card class={ ' col col-12 col-md-4 col-paddingtop ' } imagem={imagem1} alt={'imagem1'} />
                        <Card class={ ' col col-12 col-md-4 col-paddingtop ' } imagem={imagem1} alt={'imagem1'} />
                        <Card class={ ' col col-12 col-md-4 col-paddingtop ' } imagem={imagem1} alt={'imagem1'} />
                    </div>
                    <div className='row'>
                        <Card class={ ' col col-12 col-md-4 col-paddingtop ' } imagem={imagem2} alt={'imagem2'} />
                        <Card class={ ' col col-12 col-md-4 col-paddingtop ' } imagem={imagem2} alt={'imagem2'} />
                        <Card class={ ' col col-12 col-md-4 col-paddingtop ' } imagem={imagem2} alt={'imagem2'} />
                    </div>                
                </section>
                <div text align='center'>
                    <Artigo titulo={<h3>Título</h3>} />
                    <Button class={ 'button button-green' } href={'#'} text={'Saiba Mais'} />
                </div>
                <Footer />
            </React.Fragment>
        )
    }
}