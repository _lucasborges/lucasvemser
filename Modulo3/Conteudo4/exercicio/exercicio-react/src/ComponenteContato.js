import React, { Component } from 'react';
import Header from './componentes/Header-componente/Header';
import Footer from './componentes/Footer-componente/Footer';
import Artigo from './componentes/Artigo-componente/Artigo';
import Campo from './componentes/Imput-componente/Campo';
import Button from './componentes/Button-componente/Buttons';
import './componentes/Imput-componente/contato.css'
import './layout-css/grid.css';

export default class ComponenteContato extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <section className='container'>
                    <div className='row'>
                        <div className='col col-12 col-md-6 col-paddingtop'>
                            <Artigo titulo={<h3>Título</h3>} className='col-paddingtop' />
                            <form className='clearfix'>
                                <Campo classe={'field'} tipo={'text'} nome={'Nome'} />
                                <Campo classe={'field'} tipo={'text'} nome={'E-mail'} />
                                <Campo classe={'field'} tipo={'text'} nome={'Assunto'} />
                                <textarea className='field' placeholder='Mensagem' />
                                <Button class={'button button-green'} href={'#'} text={'Enviar'}/>  
                            </form>
                        </div>
                    </div>
                    <div className="map-container">
                        <iframe className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7200844760855!2d-51.17087028474702!3d-30.01619288189262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576870098547!5m2!1spt-BR!2sbr" allowfullscreen=""></iframe>
                    </div>
                </section>
                <Footer />
            </React.Fragment>
        )
    }
}