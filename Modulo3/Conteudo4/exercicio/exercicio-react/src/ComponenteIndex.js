import React, { Component } from 'react';

import imagem1 from './componentes/Card-componente/imagem1.png';
import imagem2 from './componentes/Card-componente/imagem2.png';
import imagem3 from './componentes/Card-componente/imagem3.png';
import imagem4 from './componentes/Card-componente/imagem4.png';
import Banner from './componentes/Banner-componente/Banner';
import Card from './componentes/Card-componente/Card';
import Footer from './componentes/Footer-componente/Footer';
import Header from './componentes/Header-componente/Header';
import Artigo from './componentes/Artigo-componente/Artigo';

export default class ComponenteIndex extends Component {
    render () {
        return (
        <React.Fragment>
            <Header />
            <Banner />
            <section className="container">
                <div className="row">
                    <article className="col col-12 col-md-7 col-lg-7">
                        <Artigo titulo={<h2>Título</h2>} />
                        </article>
                        <article className="col col-12 col-md-5 col-lg-5">
                        <Artigo titulo={<h2>Título</h2>} />
                    </article>
                </div>
                <div className="row">
                    <Card class={'col col-12 col-md-6 col-lg-3'} imagem={imagem1} alt={'imagem1'}/>
                    <Card class={'col col-12 col-md-6 col-lg-3'} imagem={imagem2} alt={'imagem2'}/>
                    <Card class={'col col-12 col-md-6 col-lg-3'} imagem={imagem3} alt={'imagem3'}/>
                    <Card class={'col col-12 col-md-6 col-lg-3'} imagem={imagem4} alt={'imagem4'}/>
                </div>
            </section>
            <Footer />
        </React.Fragment>    
        )
    }
}