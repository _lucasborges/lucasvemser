import React, { Component } from 'react';
import './App.css';
import ListaSeries from './ListaSeries';


export default class App extends Component {
  constructor( props ){
    super (props);
    this.listaSeries = new ListaSeries();

    console.log( this.listaSeries.invalidas() );
    console.log( this.listaSeries.filtrarPorAno( 2017 ) );
    console.log( this.listaSeries.procuraPorNome("Rami Malek"));
    console.log( this.listaSeries.mediaDeEpisodios());
    console.log( this.listaSeries.totalSalarios( 1 ));
    console.log( this.listaSeries.queroGenero("Drama") );
    console.log( this.listaSeries.queroTitulo( "The" ) );
    console.log( this.listaSeries.creditos( 1 ) );
    console.log(this.listaSeries.mostrarPalavraSecreta())
    //console.log( this.listaSeries.abreviados() );
    //console.log( this.listaSeries.elencoAbreviado() );
  }

  render() {
    return(
      <div>

      </div>
    )
  }

}
