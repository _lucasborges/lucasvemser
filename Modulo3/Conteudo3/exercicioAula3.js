/*
    Exercício 01

Criar uma função que receba um multiplicador e vários valores a serem multiplicados, o retorno deve ser um array!
Ex.:
multiplicar(5, 3, 4) // [15, 20]                                                                                                        multiplicar(5, 3, 4, 5) // [15, 20, 25]
*/

function multiplicar( multiplicador, ...valores ) {
    valores.forEach(valor => {
        console.log(valor * multiplicador)
    })
    /* let resultado = 0;
    let resultadoFinal = [];
    for ( let i = 0; i < valores.length; i++ ) {
        resultado = multiplicador * valores[i];
        resultadoFinal[i] = resultado;
    }

    return resultadoFinal; */
}

//console.log(multiplicar(5, 3, 4, 20));

multiplicar(5, 3, 4, 20)

// com map

function multiplicar2 ( multiplicador, ...valores ) {
    return resultado = valores.map( valor => valor * multiplicador );
}

console.log(multiplicar2(5, 3, 4, 20));

/*
Exercício 02

Criar um formulário de contato no site de vocês (Nome, email, telefone, assunto), ao sair do input de nome tem que garantir que tenha no minimo 10 caracteres, garantir que em email tenha @ e ao clicar no botão de enviar tem que garantir que não tenha campos em branco.

*/

let inputName = document.getElementById('name');
inputName.addEventListener('blur', () => {
    if ( inputName.value.length < 10 ){
        alert ("Preencher nome completo!");
    };
});


let inputEmail = document.getElementById('email');
inputEmail.addEventListener('blur', () => {
    if ( inputEmail.value.indexOf('@') == -1 || inputEmail.value == "" ){
        alert ("Email inválido!");
        document.inputEmail.focus();
    };
});

let inputCampos = document.getElementsByClassName('field');
let inputEnviar = document.getElementById('btn-check');
inputEnviar.addEventListener('click', () => {
    for ( var i = 0; i < inputCampos.length; i++ ){
        if ( inputCampos[i].value.length < 1 ) {                    
            alert ("preencher campos que estiverem vazio!");
        }
    }    
});

/*Marcos
function validacao(e){
    let target = e.target;
    let msg = '';
    switch (target.name) {
        case "nome":
            if(target.value == ''){
                msg = "Esse campo não pode ser vazio";
            } else {
                msg = '';
            }            
            break;
        case "email":
            break;
        default:
            break;        
    }
    let erro = document.getElementById(`msg-erro-${target.name}`)
    erro.innerHTML = msg;
}


let inputCampos = document.getElementsByClassName('field');
for ( let i = 0; i < inputCampos.length; i++ ) {
    inputCampos[i].addEventListener('blur', (e) => validacao(e));
}
*/