//ultilização do spread

//alterar funcão e paramentros sem alterar a chamada.

function criarSanduiche( pao, recheio, queijo ) {
    console.log(
        `Seu sanduiche tem o pão ${pao} 
         com recheio de ${recheio} 
         e queijo ${queijo}`);
}

const ingredientes = ['3 queijos','Frango','Cheddar'];
criarSanduiche(...ingredientes);

//para quando não tenho valores definidos, transformas os valores em um array

function receberValoresIndefinidos(...valores) {
    valores.map(valor => console.log(valor));
}

receberValoresIndefinidos(1, 2, 3, 4, 5, 6);

// funciona também com string console.log(..."Marcos") 
// transforma em array console.log([..."Marcos""])
// transforma em um objeto console.log({..."Marcos"})

//eventos

//window or document

let inputTeste = document.getElementById('campoTeste');
inputTeste.addEventListener('blur', () => {
    alert("Obrigado");
});