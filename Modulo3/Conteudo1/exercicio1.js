/*
    Exercício 01

Crie uma função "calcularCirculo" que receba um objeto com os seguintes parâmetros:

{
    		raio, // raio da circunferência
    		tipoCalculo // se for "A" cálcula a área, se for "C" calcula a circunferência
	}

*/


let circulo = {
    raio: 2,
    tipoCalculo:"a"    
}

function calcularCirculo( circulo ) {
    if ( circulo.tipoCalculo == "a" ){
     console.log(circulo.raio * circulo.raio * Math.PI);   
    }
    else {
        console.log(2 * Math.PI * circulo.raio);
    }    
}

calcularCirculo( circulo );

function calcularCirculo2 ( { raio, tipoCalculo } ) {
    if ( tipoCalculo == "a" ){
        console.log(raio * raio * Math.PI);   
       }
       else {
           console.log(2 * Math.PI * raio);
       }
}

calcularCirculo2( {raio: 3, tipoCalculo: "c"} );

/*Marcos
    function calcularCirculo({raio, tipoCalculo:tipo}){
        return Math.ceil(tipo == "A" ? Math.PI * Math.pow( raio, 2 ): Math.pow ( Math.PI, 2 ) * raio);
    }
*/


/*
    Exercício 02

Crie uma função naoBissexto que recebe um ano (número) e verifica se ele não é bissexto. Exemplo:

	naoBissexto(2016) // false
	naoBissexto(2017) // true

*/


function naoBissexto (ano) {
    if ( ano % 4 === 0 && ano % 100 !== 0 || ano % 400 === 0) {
        console.log(false);
    }
    else {
        console.log(true);
    }
}

naoBissexto(2016);
naoBissexto(2017);


/*Marcos
    function naoBissexto(ano){
        return (ano % 400 === 0) || (ano % 4 === 0 && ano % 400 === 0) ? false : true;
    }

    EXEMPLO funcao na constante

    const test = {
        diaAula : "Segundo",
        local: "DBC",
        bissexto(ano) {
            return (ano % 400 === 0) || (ano % 4 === 0 && ano % 400 === 0) ? true : false;
        }            
    }

    EXEMPLO ARROWFUNCTION

    let bissexto = ano => {
        return (ano % 400 === 0) || (ano % 4 === 0 && ano % 400 === 0) ? true : false;
    }

    OU

    let bissexto = ano => (ano % 400 === 0) || (ano % 4 === 0 && ano % 400 === 0);
    

    console.log( bissexto(2016) )
*/


/*
    Exercício 03

Crie uma função somarPares que recebe um array de números (não precisa fazer validação) e soma todos números nas posições pares do array, exemplo:

somarPares( [ 1, 56, 4.34, 6, -2 ] ) // 3.34

*/

function somarPares( valor ) {
    let som = 0;
    for ( i = 0; i < valor.length; i++ ) {
        if ( i % 2 === 0 ) {
            som += valor[i];
        }
    }
    console.log(som);
}

somarPares([ 1, 56, 4.34, 6, -2 ]);


/*Marcos
    function somarPares( valor ) {
    let som = 0;
    for ( i = 0; i < valor.length; i++ ) {
        if ( i % 2 === 0 ) {
            som += valor[i];
        }
    }
    return som;
}
*/



/*
    Exercício 04 - Soma diferentona

Escreva uma função adicionar que permite somar dois números através de duas chamadas diferentes (não necessariamente pra mesma função). Pirou? Ex:

adicionar(3)(4) // 7
adicionar(5642)(8749) // 14391

*/

function adicionar( valor1 ) {
    function adicionar2( valor2 ) {
        return valor1 + valor2;
    }
    return adicionar2
}

console.log(adicionar(5642)(8749));

/*Marcos
    function adicionar(op1){
        return function(op2){
            return op1 + op2;
        }
    }

    let adicionar = op1 => op2 => op1 + op2;


    EXEMPLO USO DO CURRY

    const divisivelPor = divisor => numero => !(numero % divisor);
    const is_divisivel = divisivelPor(2);
    console.log(is_divisivel(20));
    console.log(is_divisivel(11));
    console.log(is_divisivel(12));

*/

/*
Exercício 05

Escreva uma função imprimirBRL que recebe um número flutuante (ex: 4.651) e retorne como saída uma string no seguinte formato (seguindo o exemplo): “R$ 4,66”

Outros exemplos:

imprimirBRL(0) // “R$ 0,00”
imprimirBRL(3498.99) // “R$ 3.498,99”
imprimirBRL(-3498.99) // “-R$ 3.498,99”
imprimirBRL(2313477.0135) // “R$ 2.313.477,02”

OBS: note que o arredondamento sempre deve ser feito para cima, em caso de estourar a precisão de dois números
OBS: o separador decimal, no Brasil, é a vírgula
OBS: o separador milhar, no Brasil, é o ponto

*/

function imprimirBRL( valor ){
    return (valor.toLocaleString('pt-BR', {style: 'currency', currency:'BRL'}));
}

console.log(imprimirBRL(2313477.0135));



