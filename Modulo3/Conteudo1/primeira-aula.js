//console.log("Cheguei", "Aqui");
/*
console.log(nomeDaVariavel); // a var não foi inicializada, não acusa o erro.

nomeDaVariavel = "valor"; // global não é mais usado
var nomeDaVariavel = "valor"; // local não é mais usado
let nomeDaLet = "valorDaLet"; // tem scopo local
const nomeDaConst = "valorDaConst"; // tem scopo local e é constante não pode ser atribuido outro valor

var nomeDaVariavel = "valor3"; // atribui um novo valor pra var
nomeDaLet = "valorLet2"; // atribui um novo valor pra let

console.log ( nomeDaVariavel );
console.log ( nomeDaLet );
console.log (nomeDaConst);


//declarar objeto
const nomeDaConst2 = {
    nome: "Marcos",
    idade: 29
}
console.log ( nomeDaConst2 );


nomeDaConst2.nome = "Marcos Henrique" //atribui outro valor para a constante do objeto (burlando a constante)

console.log ( nomeDaConst2 );


Object.freeze( nomeDaConst2 ); // impede que qualquer valor seja alterado.

nomeDaConst2.nome = "Lucas"

console.log ( nomeDaConst2 );


function nomeDaFunction() {
    //nomeDaVariavel = "valor";
    let nomeDaLet = "valorLet2";//scopo local só na função 
}

console.log (nomeDaLet);


// Funcoes

 sem atributos
function somar() {

}


//com atributos
//recebendo 1 como valor default para o valor2;
function somar( valor1, valor2 = 1 ) {
    console.log( valor1 + valor2 );
}

somar( 3 );
somar( 3, 2 );

console.log ( 1 + 1 ); 
console.log ( 1 + "1" ); // concatenar


function ondeMoro( cidade ) {
    //console.log("Eu moro em " + cidade + ". E sou feliz!");
    console.log (`Eu moro em ${ cidade }. E sou feliz ${ 30 + 1 } dias!`);
}

ondeMoro("Gravataí");


function fruteira() {
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                + "Goiaba"
                + "\n"
                + "Pessego"
                + "\n";
    
    let newTexto = `
    Banana
        Ameixa
            Goiaba
                Pessego
                `;
        console.log (texto);
    console.log (newTexto);
}

fruteira();

*/
let eu = {
    nome: "Marcos",
    idade: 29,
    altura: 1.85
};
/*
function quemSou( pessoa ){
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} anos e ${pessoa.altura} de altura.`);
}
quemSou(eu);


let funcaoSomarValores = function( a, b ){
    return a + b;
}

let add = funcaoSomarValores
let resultado = add( 3, 5 )
console.log(resultado); 


//Desctruction quebra o objeto em parametros, mas obejto continua existindo, funciona também com array;
//const { nome, idade } = eu
const { nome:n, idade:i } = eu
//console.log(nome, idade);
console.log(n, i);


// com array a posicao importa;

const array = [ 1, 3, 4, 8 ];
const [ n1, n2, n3, n4 = 18  ] = array;
console.log( n1, n2, n3, n4 );


function testarPessoa({ nome, idade, altura }){
    console.log(nome, idade, altura);
}

testarPessoa(eu);



//inverter valores com destruction

let a1 = 42;
let b1 = 15;
let c1 = 99;
let d1 = 109;

console.log(a1, b1, c1, d1);

[a1, b1, c1, d1] = [b1, d1, a1, c1];

console.log(a1, b1, c1, d1);

*/
