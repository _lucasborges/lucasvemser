import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Tela from './Componentes/Login/Tela-login';
import Menu from './Componentes/Menu/Menu';
import Agencias from './Componentes/Agencias/Agencias';
import TiposDeContas from './Componentes/TiposDeContas/TiposDeContas';
import Clientes from './Componentes/Clientes/Clientes';
import ContaClientes from './Componentes/ContaClientes/ContaClientes';
import AgenciaEspecifica from './Componentes/Agencias/AgenciaEspecifica';
import ClienteEspecifico from './Componentes/Clientes/ClienteEspecifico';
import ContaClienteEspecifica from './Componentes/ContaClientes/ContaClienteEspecifica';

function App() {
  return (
    <div className="App">
      <Router>
        <Route path="/" exact component={ Tela }/> 
        <Route path="/menu" component={ Menu }/>     
        <Route path="/agencias" component={ Agencias }/> 
        <Route path="/agencia/:id" exact component={ AgenciaEspecifica }/>     
        <Route path="/tipocontas" component={ TiposDeContas }/>
        <Route path="/clientes" component={ Clientes }/>
        <Route path="/cliente/:id" exact component={ ClienteEspecifico }/>
        <Route path="/conta/clientes" component={ ContaClientes }/>
        <Route path="/conta/cliente/:id" exact component={ ContaClienteEspecifica }/>
      </Router>
    </div>
  );
}

export default App;
