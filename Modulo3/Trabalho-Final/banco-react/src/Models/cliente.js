export default class Cliente {
    constructor( id, nome, cpf, idAgencia, codAgencia, nomeAgencia ) {
        this.id = id
        this.nome = nome
        this.cpf = cpf
        this.idAgencia = idAgencia
        this.codAgencia = codAgencia
        this.nomeAgencia = nomeAgencia
    }

    getNomeClientes() {
        return `ID: ${ this.id } - Nome: ${ this.nome }`
    }
}