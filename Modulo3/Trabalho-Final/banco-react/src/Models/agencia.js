export default class Agencia {
    constructor( id, codigo, nome, logradouro, numero, bairro, cidade, uf ) {
        this.id = id
        this.codigo = codigo
        this.nome = nome
        this.logradouro = logradouro
        this.numero = numero
        this.bairro = bairro
        this.cidade = cidade
        this.uf = uf
    }

    getNome() {
        return `${this.nome}`
    }

    getEndereco(){
        return `Nome: ${ this.nome }
                End: ${ this.logradouro }, ${ this.numero }
                Bairro: ${ this.bairro }/${ this.uf }`
    }
}