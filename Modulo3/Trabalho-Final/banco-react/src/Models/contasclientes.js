export default class ContasClientes {
    constructor( id, codigo, tpId, tpNome, cId, cNome, cCpf ) {
        this.id = id
        this.codigo = codigo
        this.tpId = tpId
        this.tpNome = tpNome
        this.cId = cId
        this.cNome = cNome
        this.cCpf = cCpf
    }

    getDadosContasClientes() {
        return `Código do cliente: ${ this.cId } - cliente: ${ this.cNome }`
    }
}