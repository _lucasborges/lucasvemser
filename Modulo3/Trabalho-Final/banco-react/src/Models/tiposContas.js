export default class tiposContas {
    constructor( id, nome ){
        this.id = id
        this.nome = nome
    } 

    getDadosTpContas(){
        return `Id: ${ this.id } - Tipo: ${ this.nome }`
    }
}