import axios from 'axios';

export default class Api {


    getAgencias = () => axios.get('http://localhost:1337/agencias', { 
        headers: { 
            Authorization: "banco-vemser-api-fake" 
        } 
      })


    getTipoContas = () => axios.get('http://localhost:1337/tipoContas', { 
        headers: { 
            Authorization: "banco-vemser-api-fake" 
        } 
    })


    getClientes = () => axios.get('http://localhost:1337/clientes', {
        headers: {
            Authorization: "banco-vemser-api-fake"
        }
    })

    getContaClientes = () => axios.get('http://localhost:1337/conta/clientes', {
        headers: {
            Authorization: "banco-vemser-api-fake"
        }
    })

}

