import React from 'react';

import './Card.css';
import Botao from '../Botao/Botao';
import Campo from '../Input/Campo';


export default ( props ) => 
                <div className={ props.classe }>
                    <article className="box" >
                        <p>Welcome</p>
                        <Campo classe="field" tipo="text" holder="Email@example.com" />
                        <Campo classe="field" tipo="text" holder="Password" />
                        <Botao classe="button button-green" href="#" texto="LOGIN" />
                        <p>Não tem uma conta? <a>Sign Up!</a></p>
                    </article>
                </div>