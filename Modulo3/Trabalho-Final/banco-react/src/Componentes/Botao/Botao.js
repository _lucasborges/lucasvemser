import React from 'react';
import './Botao.css'

export default ( props ) =>
            <React.Fragment>
                <button className={ props.classe } href={ props.href }>{props.texto}</button>
            </React.Fragment>
       
