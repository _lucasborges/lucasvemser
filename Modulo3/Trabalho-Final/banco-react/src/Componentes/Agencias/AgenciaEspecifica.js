import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';
import '../Layout-css/grid.css';
import Api from '../../service-api/Api';
import Botao from '../Botao/Botao';
import './Agencia.css'


export default class AgenciaEspecifica extends Component {
    constructor(props) {
        super(props);
        this.banco = new Api();
        this.state = {
            agencia: {}
        }
    }  



    render() {
        const { agencia } = this.props.location.state
        return (
            <React.Fragment>
                <section className=" container ">
                    <div className="row">
                            <div className ="col col-4 col-offset-4 App-header">
                                <article className="box-menu" >
                                    <div className="box-interna">
                                        <h2>Dados da Agencia {agencia.nome}:</h2>
                                        <h4>Código: {agencia.codigo}</h4>
                                        <h4>Endereço: {agencia.logradouro},{agencia.numero} </h4>
                                        <h4>Cidade: {agencia.cidade}/{agencia.uf}</h4>
                                    </div> 
                                    <Link to ={{ pathname: "/agencias" }}><Botao classe="button button-blue" href="#" texto="Agencias" /></Link>                                   
                                    <Link to ={{ pathname: "/menu" }}><Botao classe="button button-blue button-duo" href="#" texto="Menu" /></Link>
                                </article> 
                            </div>
                        </div>
                </section>
            </React.Fragment>
        )
    }
    
}