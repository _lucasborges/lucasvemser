import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';
import '../Layout-css/grid.css';
import Api from '../../service-api/Api';
import Agencia from '../../Models/agencia';
import Botao from '../Botao/Botao';
import './Agencia.css'


export default class Agencias extends Component {
    constructor( props ) {
        super(props);
        this.api = new Api();
        this.state = {
            agencias: []
        }
    } 

    componentDidMount() {
        this.ag = this.buscaAgencias()
        this.ag = null
    }

    componentWillUnmount() {
        if ( this.ag ) {
            this.ag.cancel()
        }
    }

    buscaAgencias = () => {
        return this.api.getAgencias()
          .then(value => this.setState( {
            agencias: value.data.agencias.map(objAgencia => new Agencia( objAgencia.id, objAgencia.codigo, objAgencia.nome, objAgencia.endereco.logradouro, objAgencia.endereco.numero, objAgencia.endereco.bairro, objAgencia.endereco.cidade, objAgencia.endereco.uf ))} )
          )
          .catch( "Nao foi possivel carregar o conteúdo" )
      }
    
    render() {

        const { agencias } = this.state
        console.log(agencias);
        return (
            <React.Fragment>
                <section className=" container ">
                    <div className="row">
                            <div className ="col col-4 col-offset-4 App-header">
                                <article className="box-menu" >
                                    <div className="box-interna">
                                        <ul>Nossas Agências:
                                            { agencias.map((elemento, i) =><Link to={{ pathname:`agencia/${elemento.id}`, state:{ agencia: elemento } }}> <li className="listas" key={i}> {elemento.getNome()} </li></Link>)}
                                        </ul>
                                    </div>                                    
                                    <Link to ={{ pathname: "/menu" }}><Botao classe="button button-blue" href="#" texto="Menu" /></Link>
                                </article> 
                            </div>
                        </div>
                </section>
            </React.Fragment>
        )
    }
}