import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';
import TiposContas from '../../Models/tiposContas';
import '../Layout-css/grid.css';
import Api from '../../service-api/Api';
import Botao from '../Botao/Botao';

export default class TiposDeContas extends Component {
    constructor( props ) {
        super(props);
        this.api = new Api();
        this.state = {
            contas: []
        }
    } 

    componentDidMount(){
        this.tpContas = this.buscaTipoContas()
        this.tpContas = null
    }


    componentWillUnmount() {
        if ( this.tpContas ) {
            this.tpContas.cancel()
        }
    }

    buscaTipoContas = () => {
        return this.api.getTipoContas()
          .then(value => this.setState( {
            contas: value.data.tipos.map(objTipos => new TiposContas( objTipos.id, objTipos.nome ))} )
          )
          .catch( "Nao foi possivel carregar o conteúdo" )
      }


      render() {
        const { contas } = this.state
        console.log(contas)
        return (
            <React.Fragment>
                <section className=" container ">
                    <div className="row">
                        <div className ="col col-4 col-offset-4 App-header">
                            <article className="box-menu" >
                                <div className="box-interna" >
                                    <ul>Nossos Tipos de contas:
                                        { contas.map((elemento, i) => <li className="listas" key={i}> {elemento.getDadosTpContas()} </li>)}
                                    </ul>
                                </div>                                    
                                <Link to ={{ pathname: "/menu" }}><Botao classe="button button-blue " href="#" texto="Menu" /></Link>
                            </article> 
                        </div>
                    </div>
                </section>
            </React.Fragment>
        )
    }

}