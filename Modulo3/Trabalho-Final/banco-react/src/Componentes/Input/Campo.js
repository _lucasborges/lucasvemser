import React from 'react';
import './Campo.css';

export default ( props ) => 
                <React.Fragment>
                    <input name={props.name} id={props.id} className={ props.classe } type={ props.tipo } placeholder={ props.holder } />
                </React.Fragment>