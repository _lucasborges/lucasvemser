import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Botao from '../Botao/Botao';
import '../Layout-css/grid.css';
import './Menu.css';


export default class Menu extends Component {
    
    render() {
        return (
            <React.Fragment>
                <section className=" container ">
                    <div className="row">
                        <div className ="col col-4 col-offset-4 App-header">
                            <article className="box-menu" >                                    
                                <h2>Bem-Vindo</h2>
                                <Link to={{ pathname: "/agencias" }}><Botao classe="button button-blue button-menu" href="#" texto="Agências" /></Link>
                                <Link to={{ pathname: "/tipocontas" }}><Botao classe="button button-blue button-menu" href="#" texto="Tipos de Contas" /></Link>
                                <Link to={{ pathname: "/clientes" }}><Botao classe="button button-blue button-menu" href="#" texto="Clientes" /></Link>
                                <Link to={{ pathname: "/conta/clientes" }}><Botao classe="button button-blue button-menu" href="#" texto="Conta de clientes" /></Link>
                            </article> 
                        </div>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}