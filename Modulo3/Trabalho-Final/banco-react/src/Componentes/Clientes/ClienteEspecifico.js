import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import '../Layout-css/grid.css';
import Api from '../../service-api/Api';
import Botao from '../Botao/Botao';


export default class AgenciaEspecifica extends Component {
    constructor(props) {
        super(props);
        this.banco = new Api();
        this.state = {
            cliente: {}
        }
    }


    render() {
        const { cliente } = this.props.location.state
        return (
            <React.Fragment>
                <section className=" container ">
                    <div className="row">
                            <div className ="col col-4 col-offset-4 App-header">
                                <article className="box-menu" >
                                    <div className="box-interna">
                                        <h2>Dados do cliente {cliente.nome}:</h2>
                                        <h4>CPF: {cliente.cpf}</h4>
                                        <h4>Agencia: {cliente.nomeAgencia}</h4>
                                        <h4>Codigo da agência: {cliente.codAgencia} </h4>
                                    </div> 
                                    <Link to ={{ pathname: "/clientes" }}><Botao classe="button button-blue" href="#" texto="Clientes" /></Link>                                   
                                    <Link to ={{ pathname: "/menu" }}><Botao classe="button button-blue button-duo" href="#" texto="Menu" /></Link>
                                </article> 
                            </div>
                        </div>
                </section>
            </React.Fragment>
        )
    }
    
}