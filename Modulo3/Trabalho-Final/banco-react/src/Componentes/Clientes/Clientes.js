import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';
import Cliente from '../../Models/cliente';
import '../Layout-css/grid.css';
import Api from '../../service-api/Api';
import Botao from '../Botao/Botao';


export default class Clientes extends Component {    
        constructor( props ) {
            super(props);
            this.api = new Api();
            this.state = {
                clientes: []
            }
        }

        
        componentDidMount(){
            this.client = this.buscaClientes()
            this.client = null
        }
        
        componentWillUnmount() {
            if ( this.client ) {
                this.client.cancel()
            }
        }

        buscaClientes = () => {
            return this.api.getClientes()
            .then(value => this.setState({
                clientes: value.data.clientes.map(objCliente => new Cliente( objCliente.id, objCliente.nome, objCliente.cpf, objCliente.agencia.id, objCliente.agencia.codigo, objCliente.agencia.nome ))
            }))
            .catch( "Nao foi possivel carregar o conteúdo" ) 
        }

        render() {
            const { clientes } = this.state
            console.log(clientes)
            return (
                <React.Fragment>
                    <section className=" container ">
                        <div className="row">
                            <div className ="col col-4 col-offset-4 App-header">
                                <article className="box-menu" >  
                                    <div className="box-interna" >
                                        <ul>Nossos Clientes:
                                           { clientes.map((elemento, i) => <Link to={{ pathname:`cliente/${elemento.id}`, state:{ cliente: elemento } }}><li className="listas" key={i}> {elemento.getNomeClientes()} </li></Link> ) }
                                        </ul>
                                    </div>                                  
                                    <Link to ={{ pathname: "/menu" }}><Botao classe="button button-blue" href="#" texto="Menu" /></Link>
                                </article> 
                            </div>
                        </div>
                    </section>
                </React.Fragment>
            )
        }

        
}