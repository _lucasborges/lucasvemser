import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';
import '../Layout-css/grid.css';
import Api from '../../service-api/Api';
import Botao from '../Botao/Botao';
import ContasClientes from '../../Models/contasclientes';

export default class ContaClientes extends Component {
    constructor( props ) {
        super(props);
        this.api = new Api();
        this.state = {
            contaClientes: []
        }
    }



    componentDidMount(){
        this._asyncRequest = this.buscaContaClientes()
        this._asyncRequest = null
    }


    componentWillUnmount() {
        if ( this._asyncRequest ) {
            this._asyncRequest.cancel()
        }
    }


    buscaContaClientes = () => {
        return this.api.getContaClientes()
          .then(value => this.setState( {
            contaClientes: value.data.cliente_x_conta.map( obj => new ContasClientes( obj.id, obj.codigo, obj.tipo.id, obj.tipo.nome, obj.cliente.id, obj.cliente.nome, obj.cliente.cpf ))
        }))
          .catch( "Nao foi possivel carregar o conteúdo" )
      }

      render() {
        const { contaClientes } = this.state
        console.log(contaClientes)
        return (
            <React.Fragment>
                <section className=" container ">
                    <div className="row">
                        <div className ="col col-4 col-offset-4 App-header">
                            <article className="box-menu" >
                                <div className="box-interna" >
                                    <ul>Contas dos nossos clientes:
                                        { contaClientes.map((elemento, i) => <Link to={{ pathname:`/conta/cliente/${elemento.id}`, state:{ conta: elemento } }}><li className="listas" key={i}> {elemento.getDadosContasClientes()} </li></Link>)}
                                    </ul>
                                </div>                                    
                                <Link to ={{ pathname: "/menu" }}><Botao classe="button button-blue " href="#" texto="Menu" /></Link>
                            </article> 
                        </div>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}