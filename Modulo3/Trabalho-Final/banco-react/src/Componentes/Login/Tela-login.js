import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';
import Botao from '../Botao/Botao';
import Campo from '../Input/Campo';
import '../Layout-css/grid.css';
import '../Card/Card.css';

export default class TelaLogin extends Component {
    
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { 
            username: null, 
            password: null 
        };
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.username = e.target.username.value;
        this.password = e.target.password.value;
        axios.post('http://localhost:1337/login', {email:this.username, senha:this.password} )
            .then((res) => {
                console.log(res.data);
            }).catch((err) => {
                console.log(err);
            })
    }

    render() {
        return (
            <React.Fragment>
                <section className=" container ">
                    <div className="row">
                        <div className ="col col-4 col-offset-4 App-header">
                            <form onSubmit={this.handleSubmit} className="box box-menu" >
                                <h1>Banco React</h1>
                                <h3>Conectar-se:</h3>
                                <Campo name="username" id="username" classe="field" tipo="email" holder="Email@example.com" />
                                <Campo name="password" id="password" classe="field" tipo="password" holder="Password" />
                                <Link to ={{ pathname: "/menu" }}><Botao classe="button button-green" href="#" texto="LOGIN" /></Link>                                
                            </form> 
                        </div>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}